(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-header></app-header>\n<app-nav></app-nav>\n\n<router-outlet></router-outlet>\n\n<app-footer></app-footer>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'addToCart';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/nav/nav.component */ "./src/app/components/nav/nav.component.ts");
/* harmony import */ var _components_home_home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/home/home.component */ "./src/app/components/home/home.component.ts");
/* harmony import */ var _components_slider_slider_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/slider/slider.component */ "./src/app/components/slider/slider.component.ts");
/* harmony import */ var _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./components/footer/footer.component */ "./src/app/components/footer/footer.component.ts");
/* harmony import */ var _components_home_items_home_items_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/home-items/home-items.component */ "./src/app/components/home-items/home-items.component.ts");
/* harmony import */ var _components_header_header_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _components_tv_tv_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/tv/tv.component */ "./src/app/components/tv/tv.component.ts");
/* harmony import */ var _components_mobile_mobile_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/mobile/mobile.component */ "./src/app/components/mobile/mobile.component.ts");
/* harmony import */ var _components_laptop_laptop_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/laptop/laptop.component */ "./src/app/components/laptop/laptop.component.ts");
/* harmony import */ var _components_item_item_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/item/item.component */ "./src/app/components/item/item.component.ts");
/* harmony import */ var _components_cart_cart_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/cart/cart.component */ "./src/app/components/cart/cart.component.ts");
















var appRoutes = [{
        path: "televizori", component: _components_tv_tv_component__WEBPACK_IMPORTED_MODULE_11__["TvComponent"]
    },
    { path: "", component: _components_home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"] },
    {
        path: "mobilni", component: _components_mobile_mobile_component__WEBPACK_IMPORTED_MODULE_12__["MobileComponent"]
    },
    {
        path: "laptopovi", component: _components_laptop_laptop_component__WEBPACK_IMPORTED_MODULE_13__["LaptopComponent"],
    },
    {
        path: "item/:name/:id/:rand", component: _components_item_item_component__WEBPACK_IMPORTED_MODULE_14__["ItemComponent"],
    }, {
        path: "cart", component: _components_cart_cart_component__WEBPACK_IMPORTED_MODULE_15__["CartComponent"],
    },
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _components_nav_nav_component__WEBPACK_IMPORTED_MODULE_5__["NavComponent"],
                _components_home_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"],
                _components_slider_slider_component__WEBPACK_IMPORTED_MODULE_7__["SliderComponent"],
                _components_footer_footer_component__WEBPACK_IMPORTED_MODULE_8__["FooterComponent"],
                _components_home_items_home_items_component__WEBPACK_IMPORTED_MODULE_9__["HomeItemsComponent"],
                _components_header_header_component__WEBPACK_IMPORTED_MODULE_10__["HeaderComponent"],
                _components_tv_tv_component__WEBPACK_IMPORTED_MODULE_11__["TvComponent"],
                _components_mobile_mobile_component__WEBPACK_IMPORTED_MODULE_12__["MobileComponent"],
                _components_laptop_laptop_component__WEBPACK_IMPORTED_MODULE_13__["LaptopComponent"],
                _components_item_item_component__WEBPACK_IMPORTED_MODULE_14__["ItemComponent"],
                _components_cart_cart_component__WEBPACK_IMPORTED_MODULE_15__["CartComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(appRoutes)
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/cart/cart.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/cart/cart.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".carts{\r\n\tdisplay: flex;\r\n\r\n\r\n\tmargin: 1rem auto;\r\n\tjustify-content: center;\r\n\talign-items: center;\r\n\tborder: 2px solid lightgrey;\r\n\tpadding: 0 1rem;\r\n}\r\n.btn{\r\n\tpadding: 0.4rem 2rem;\r\n\toutline: none;\r\n\tborder: none;\r\n\twidth: 70%;\r\n\r\n}\r\n.price{\r\n\tfont-size: 2rem;\r\n\tfont-weight: bold;\r\n\ttext-align: center;\r\n}\r\n.item-name{\r\n\tfont-size: 1.3rem;\r\n\tcolor: grey;\r\n\ttext-align: center;\r\n}\r\n.total{\r\n\tfont-size: 2rem;\r\n\tfont-weight: bold;\r\n}\r\n.emty{\r\n\tfont-size:2rem;\r\n\tfont-weight: bold;\r\n\ttext-align: center;\r\n}\r\n.item{\r\n\twidth: 20%;\r\n\ttext-align: center;\r\n}\r\n.finish{\r\n\ttext-align: center;\r\n}\r\n.finish button{\r\n\twidth: 30%;\r\n\tmargin: 1rem 0;\r\n}\r\n@media screen and (max-width: 780px) {\r\n\r\n.carts{\r\n\tflex-direction: column;\r\n\tpadding: 1rem 0;\r\n}\r\n.btn{\r\n\twidth: 50%\r\n}\r\n.item{\r\n\twidth: 100%;\r\n}\r\n}\r\n.modal{\r\n\tposition: fixed;\r\n\ttext-align: center;\r\n\tpadding: 0.3rem 0;\r\n\tcolor: green;\r\n\twidth: 30%;\r\n\ttop:70px;\r\n\tbackground:wheat;\r\nleft: 35%;\r\n\t\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9jYXJ0L2NhcnQuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtDQUNDLGFBQWE7OztDQUdiLGlCQUFpQjtDQUNqQix1QkFBdUI7Q0FDdkIsbUJBQW1CO0NBQ25CLDJCQUEyQjtDQUMzQixlQUFlO0FBQ2hCO0FBQ0E7Q0FDQyxvQkFBb0I7Q0FDcEIsYUFBYTtDQUNiLFlBQVk7Q0FDWixVQUFVOztBQUVYO0FBQ0E7Q0FDQyxlQUFlO0NBQ2YsaUJBQWlCO0NBQ2pCLGtCQUFrQjtBQUNuQjtBQUNBO0NBQ0MsaUJBQWlCO0NBQ2pCLFdBQVc7Q0FDWCxrQkFBa0I7QUFDbkI7QUFDQTtDQUNDLGVBQWU7Q0FDZixpQkFBaUI7QUFDbEI7QUFDQTtDQUNDLGNBQWM7Q0FDZCxpQkFBaUI7Q0FDakIsa0JBQWtCO0FBQ25CO0FBQ0E7Q0FDQyxVQUFVO0NBQ1Ysa0JBQWtCO0FBQ25CO0FBQ0E7Q0FDQyxrQkFBa0I7QUFDbkI7QUFDQTtDQUNDLFVBQVU7Q0FDVixjQUFjO0FBQ2Y7QUFFQTs7QUFFQTtDQUNDLHNCQUFzQjtDQUN0QixlQUFlO0FBQ2hCO0FBQ0E7Q0FDQztBQUNEO0FBQ0E7Q0FDQyxXQUFXO0FBQ1o7QUFDQTtBQUNBO0NBQ0MsZUFBZTtDQUNmLGtCQUFrQjtDQUNsQixpQkFBaUI7Q0FDakIsWUFBWTtDQUNaLFVBQVU7Q0FDVixRQUFRO0NBQ1IsZ0JBQWdCO0FBQ2pCLFNBQVM7O0FBRVQiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2NhcnQvY2FydC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmNhcnRze1xyXG5cdGRpc3BsYXk6IGZsZXg7XHJcblxyXG5cclxuXHRtYXJnaW46IDFyZW0gYXV0bztcclxuXHRqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuXHRhbGlnbi1pdGVtczogY2VudGVyO1xyXG5cdGJvcmRlcjogMnB4IHNvbGlkIGxpZ2h0Z3JleTtcclxuXHRwYWRkaW5nOiAwIDFyZW07XHJcbn1cclxuLmJ0bntcclxuXHRwYWRkaW5nOiAwLjRyZW0gMnJlbTtcclxuXHRvdXRsaW5lOiBub25lO1xyXG5cdGJvcmRlcjogbm9uZTtcclxuXHR3aWR0aDogNzAlO1xyXG5cclxufVxyXG4ucHJpY2V7XHJcblx0Zm9udC1zaXplOiAycmVtO1xyXG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uaXRlbS1uYW1le1xyXG5cdGZvbnQtc2l6ZTogMS4zcmVtO1xyXG5cdGNvbG9yOiBncmV5O1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4udG90YWx7XHJcblx0Zm9udC1zaXplOiAycmVtO1xyXG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5lbXR5e1xyXG5cdGZvbnQtc2l6ZToycmVtO1xyXG5cdGZvbnQtd2VpZ2h0OiBib2xkO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uaXRlbXtcclxuXHR3aWR0aDogMjAlO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uZmluaXNoe1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uZmluaXNoIGJ1dHRvbntcclxuXHR3aWR0aDogMzAlO1xyXG5cdG1hcmdpbjogMXJlbSAwO1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3ODBweCkge1xyXG5cclxuLmNhcnRze1xyXG5cdGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblx0cGFkZGluZzogMXJlbSAwO1xyXG59XHJcbi5idG57XHJcblx0d2lkdGg6IDUwJVxyXG59XHJcbi5pdGVte1xyXG5cdHdpZHRoOiAxMDAlO1xyXG59XHJcbn1cclxuLm1vZGFse1xyXG5cdHBvc2l0aW9uOiBmaXhlZDtcclxuXHR0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblx0cGFkZGluZzogMC4zcmVtIDA7XHJcblx0Y29sb3I6IGdyZWVuO1xyXG5cdHdpZHRoOiAzMCU7XHJcblx0dG9wOjcwcHg7XHJcblx0YmFja2dyb3VuZDp3aGVhdDtcclxubGVmdDogMzUlO1xyXG5cdFxyXG59Il19 */"

/***/ }),

/***/ "./src/app/components/cart/cart.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/cart/cart.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"cart\" [hidden]=\"toogle\">\n \n\n      \n      <div  *ngFor=\"let cartite of cartitem\">\n          \n        <div *ngIf=\"cartitem.length>0\" class=\"carts\">\n       <div class=\"color item\" ><img [src]=\"cartite.bck\" class=\"cart-img\"></div>\n\n             <p class=\"item-name item\">{{cartite.name |uppercase}}</p>\n\n            \n           \n          <!--   <input type=\"number\" #x (click)=\"get(x)\"> -->\n             \n              <p class=\"price item\">Price:{{cartite.price}} RSD</p>\n              <div class=\"item\">\n              \t <button (click)=\"deleteFromCart(cartite)\" class=\"btn\" style=\"background:red\" routerLink=\"/cart\">Izbaci iz kante</button>\n              </div>\n             \n        </div>\n   \n</div>\n    \n<p style=\"text-align: center;\" class=\"total\" *ngIf=\"cartitem.length>0\">Total:{{toTal}} RSD ({{number}})</p>\n<div class=\"finish\">\n      <button class=\"btn btn-primary\" *ngIf=\"cartitem.length>0\" (click)=\"finish()\">Zavrsi kupovinu</button>\n</div>\n\n\n<p *ngIf=\"cartitem.length===0\" class=\"emty\">Emty Cart</p>\n</div>\n\n<div class=\"modal\" *ngIf=\"modal\" >Uspesno izvrsena kupovina</div>"

/***/ }),

/***/ "./src/app/components/cart/cart.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/cart/cart.component.ts ***!
  \***************************************************/
/*! exports provided: CartComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartComponent", function() { return CartComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/cart.service */ "./src/app/services/cart.service.ts");
/* harmony import */ var _services_tv_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/tv.service */ "./src/app/services/tv.service.ts");
/* harmony import */ var _services_laptop_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/laptop.service */ "./src/app/services/laptop.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_mobile_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/mobile.service */ "./src/app/services/mobile.service.ts");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_7__);








var CartComponent = /** @class */ (function () {
    function CartComponent(cartService, tvService, lapService, mobileService, router) {
        this.cartService = cartService;
        this.tvService = tvService;
        this.lapService = lapService;
        this.mobileService = mobileService;
        this.router = router;
        this.toTal = 0;
        this.cartitem = [];
        this.number = 0;
        this.modal = false;
    }
    CartComponent.prototype.ngOnInit = function () {
        this.totalFromAdd();
        this.totalFromDelete();
        this.number = this.cartitem.length;
    };
    CartComponent.prototype.deleteFromCart = function (item) {
        this.cartService.del(item);
    };
    CartComponent.prototype.totalFromDelete = function () {
        var _this = this;
        this.cartService.totalCart.subscribe(function (data) { return _this.toTal = data; });
        this.cartService.lenC.subscribe(function (len) { return _this.number = len; });
    };
    CartComponent.prototype.totalFromAdd = function () {
        this.cartitem = this.cartService.cart;
        this.toTal = this.cartService.totalInCart;
    };
    CartComponent.prototype.finish = function () {
        var _this = this;
        // alert("Uspesno izvrsena kupovina,isnos za placanje je "+this.toTal+"dinara")
        this.modal = true;
        var customer = {
            items: this.cartitem,
            customerInfo: {
                name: "Bojan Bogdanovic",
                email: "bojanb106@gmial.com",
                street: "ul 422 br 3"
            },
            price: {
                totalPrice: this.toTal + "din",
            }
        };
        axios__WEBPACK_IMPORTED_MODULE_7___default.a.post("https://burger-builder-1232d.firebaseio.com/shopOrders.json", customer);
        setTimeout(function () {
            _this.cartService.resetCart();
        }, 0);
        setTimeout(function () {
            _this.modal = false;
            _this.cartService.resetCart();
        }, 3000);
    };
    CartComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-cart',
            template: __webpack_require__(/*! ./cart.component.html */ "./src/app/components/cart/cart.component.html"),
            styles: [__webpack_require__(/*! ./cart.component.css */ "./src/app/components/cart/cart.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"],
            _services_tv_service__WEBPACK_IMPORTED_MODULE_3__["TvService"],
            _services_laptop_service__WEBPACK_IMPORTED_MODULE_4__["Laptop"],
            _services_mobile_service__WEBPACK_IMPORTED_MODULE_6__["MobileService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"]])
    ], CartComponent);
    return CartComponent;
}());



/***/ }),

/***/ "./src/app/components/footer/footer.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/footer/footer.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* Footer */\r\n.row{\r\n    display: flex;\r\n    flex-direction: column;\r\n    width: 100%;\r\n \r\n    \r\n    \r\n}\r\n.row-items{\r\n    height: 40%;\r\n}\r\n.col\r\n{\r\n    display: flex;\r\n    height: 100%;\r\n  align-items: center;\r\n\r\n \r\n    \r\n}\r\n.col-items{\r\nwidth: 33%;\r\nheight: 100%;\r\ndisplay: flex;\r\nflex-direction: column;\r\nalign-items: center;\r\njustify-content: flex-start;\r\n\r\n\r\n}\r\n.col-lica{\r\n    border-right: 2px solid white;\r\n    \r\n}\r\n.lica,.col-pozivi{\r\n    margin: 1rem;\r\n}\r\n.col-news{\r\n    border-right: 2px solid white;\r\n}\r\n.fizicka{\r\n    width: 60%;\r\n}\r\nfooter{\r\n    height: 300px;\r\n    background:#2f343d;\r\n\r\n}\r\n.footer-para{\r\n    font-size: 14px;\r\n    color: black;\r\n    margin: 0.5rem;\r\n}\r\n.footer-h{\r\n    font-size: 20px;\r\n    color: grey;\r\n    margin:0.5rem;\r\n}\r\n.prijava-p{\r\n    margin: 1rem;\r\n    font-size: 23px;\r\n    color:grey;\r\n}\r\n.input{\r\n    padding: 0.7rem 1rem;\r\n    background: #23272e;\r\n    border: none;\r\n    outline: none;\r\n}\r\n.button{\r\n    \r\n        padding: 0.7rem 1rem;\r\n       background: white;\r\n        border: none;\r\n        outline: none;\r\n    \r\n}\r\n.button:hover{\r\n    background: rgb(206, 203, 10);\r\n}\r\n.footer-ul{\r\n    list-style: none;\r\n}\r\n.phone-h{\r\n    font-size: 25px;\r\n}\r\n@media screen and (max-width:768px){\r\n\r\n  \r\n\r\n\r\n\r\n    .phone-h{\r\n        text-align: center;\r\n        padding: 0.6rem 0;\r\n        ;\r\n    }\r\n    .phone-h span i{\r\n        color: green;\r\n        margin: 0 0.5rem;\r\n    }\r\n    \r\n    footer{\r\n        height: auto;\r\n        width: 100%;\r\n        min-width: auto;\r\n    }\r\n    \r\n    .col-items{\r\n        width: 50%;\r\n        text-align: center;\r\n        margin: 0.5rem;\r\n        padding-bottom: 1rem;\r\n    }\r\n    .col{\r\n        flex-direction: column;\r\n    }\r\n    \r\n    \r\n    \r\n    .col-lica,.col-news{\r\n     border: none;\r\n     border-bottom: 2px solid white;\r\n     width: 90%;\r\n    }\r\n    \r\n    .col-pozivi,.licaa{\r\n        margin: 0rem;\r\n    }\r\n  \r\n    \r\n    \r\n    \r\n    \r\n    \r\n    }\r\n\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsV0FBVztBQUNYO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtJQUN0QixXQUFXOzs7O0FBSWY7QUFFQTtJQUNJLFdBQVc7QUFDZjtBQUVBOztJQUVJLGFBQWE7SUFDYixZQUFZO0VBQ2QsbUJBQW1COzs7O0FBSXJCO0FBQ0E7QUFDQSxVQUFVO0FBQ1YsWUFBWTtBQUNaLGFBQWE7QUFDYixzQkFBc0I7QUFDdEIsbUJBQW1CO0FBQ25CLDJCQUEyQjs7O0FBRzNCO0FBR0E7SUFDSSw2QkFBNkI7O0FBRWpDO0FBRUE7SUFDSSxZQUFZO0FBQ2hCO0FBRUE7SUFDSSw2QkFBNkI7QUFDakM7QUFFQTtJQUNJLFVBQVU7QUFDZDtBQUVBO0lBQ0ksYUFBYTtJQUNiLGtCQUFrQjs7QUFFdEI7QUFHQTtJQUNJLGVBQWU7SUFDZixZQUFZO0lBQ1osY0FBYztBQUNsQjtBQUNBO0lBQ0ksZUFBZTtJQUNmLFdBQVc7SUFDWCxhQUFhO0FBQ2pCO0FBRUE7SUFDSSxZQUFZO0lBQ1osZUFBZTtJQUNmLFVBQVU7QUFDZDtBQUVBO0lBQ0ksb0JBQW9CO0lBQ3BCLG1CQUFtQjtJQUNuQixZQUFZO0lBQ1osYUFBYTtBQUNqQjtBQUtBOztRQUVRLG9CQUFvQjtPQUNyQixpQkFBaUI7UUFDaEIsWUFBWTtRQUNaLGFBQWE7O0FBRXJCO0FBRUE7SUFDSSw2QkFBNkI7QUFDakM7QUFJQTtJQUNJLGdCQUFnQjtBQUNwQjtBQUlBO0lBQ0ksZUFBZTtBQUNuQjtBQU9BOzs7Ozs7SUFNSTtRQUNJLGtCQUFrQjtRQUNsQixpQkFBaUI7O0lBRXJCO0lBQ0E7UUFDSSxZQUFZO1FBQ1osZ0JBQWdCO0lBQ3BCOztJQUVBO1FBQ0ksWUFBWTtRQUNaLFdBQVc7UUFDWCxlQUFlO0lBQ25COztJQUVBO1FBQ0ksVUFBVTtRQUNWLGtCQUFrQjtRQUNsQixjQUFjO1FBQ2Qsb0JBQW9CO0lBQ3hCO0lBQ0E7UUFDSSxzQkFBc0I7SUFDMUI7Ozs7SUFJQTtLQUNDLFlBQVk7S0FDWiw4QkFBOEI7S0FDOUIsVUFBVTtJQUNYOztJQUVBO1FBQ0ksWUFBWTtJQUNoQjs7Ozs7OztJQU9BIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9mb290ZXIvZm9vdGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBGb290ZXIgKi9cclxuLnJvd3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiBcclxuICAgIFxyXG4gICAgXHJcbn1cclxuXHJcbi5yb3ctaXRlbXN7XHJcbiAgICBoZWlnaHQ6IDQwJTtcclxufVxyXG5cclxuLmNvbFxyXG57XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcblxyXG4gXHJcbiAgICBcclxufVxyXG4uY29sLWl0ZW1ze1xyXG53aWR0aDogMzMlO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbmRpc3BsYXk6IGZsZXg7XHJcbmZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbmFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbmp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuXHJcblxyXG59XHJcblxyXG5cclxuLmNvbC1saWNhe1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAycHggc29saWQgd2hpdGU7XHJcbiAgICBcclxufVxyXG5cclxuLmxpY2EsLmNvbC1wb3ppdml7XHJcbiAgICBtYXJnaW46IDFyZW07XHJcbn1cclxuXHJcbi5jb2wtbmV3c3tcclxuICAgIGJvcmRlci1yaWdodDogMnB4IHNvbGlkIHdoaXRlO1xyXG59XHJcblxyXG4uZml6aWNrYXtcclxuICAgIHdpZHRoOiA2MCU7XHJcbn1cclxuXHJcbmZvb3RlcntcclxuICAgIGhlaWdodDogMzAwcHg7XHJcbiAgICBiYWNrZ3JvdW5kOiMyZjM0M2Q7XHJcblxyXG59XHJcblxyXG5cclxuLmZvb3Rlci1wYXJhe1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG4gICAgbWFyZ2luOiAwLjVyZW07XHJcbn1cclxuLmZvb3Rlci1oe1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgY29sb3I6IGdyZXk7XHJcbiAgICBtYXJnaW46MC41cmVtO1xyXG59XHJcblxyXG4ucHJpamF2YS1we1xyXG4gICAgbWFyZ2luOiAxcmVtO1xyXG4gICAgZm9udC1zaXplOiAyM3B4O1xyXG4gICAgY29sb3I6Z3JleTtcclxufVxyXG5cclxuLmlucHV0e1xyXG4gICAgcGFkZGluZzogMC43cmVtIDFyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiAjMjMyNzJlO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuXHJcblxyXG5cclxuLmJ1dHRvbntcclxuICAgIFxyXG4gICAgICAgIHBhZGRpbmc6IDAuN3JlbSAxcmVtO1xyXG4gICAgICAgYmFja2dyb3VuZDogd2hpdGU7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBcclxufVxyXG5cclxuLmJ1dHRvbjpob3ZlcntcclxuICAgIGJhY2tncm91bmQ6IHJnYigyMDYsIDIwMywgMTApO1xyXG59XHJcblxyXG5cclxuXHJcbi5mb290ZXItdWx7XHJcbiAgICBsaXN0LXN0eWxlOiBub25lO1xyXG59XHJcblxyXG5cclxuXHJcbi5waG9uZS1oe1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6NzY4cHgpe1xyXG5cclxuICBcclxuXHJcblxyXG5cclxuICAgIC5waG9uZS1oe1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiAwLjZyZW0gMDtcclxuICAgICAgICA7XHJcbiAgICB9XHJcbiAgICAucGhvbmUtaCBzcGFuIGl7XHJcbiAgICAgICAgY29sb3I6IGdyZWVuO1xyXG4gICAgICAgIG1hcmdpbjogMCAwLjVyZW07XHJcbiAgICB9XHJcbiAgICBcclxuICAgIGZvb3RlcntcclxuICAgICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgbWluLXdpZHRoOiBhdXRvO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuY29sLWl0ZW1ze1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIG1hcmdpbjogMC41cmVtO1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAxcmVtO1xyXG4gICAgfVxyXG4gICAgLmNvbHtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gICAgLmNvbC1saWNhLC5jb2wtbmV3c3tcclxuICAgICBib3JkZXI6IG5vbmU7XHJcbiAgICAgYm9yZGVyLWJvdHRvbTogMnB4IHNvbGlkIHdoaXRlO1xyXG4gICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5jb2wtcG96aXZpLC5saWNhYXtcclxuICAgICAgICBtYXJnaW46IDByZW07XHJcbiAgICB9XHJcbiAgXHJcbiAgICBcclxuICAgIFxyXG4gICAgXHJcbiAgICBcclxuICAgIFxyXG4gICAgfVxyXG5cclxuXHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/footer/footer.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/footer/footer.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<footer>\n\n\n<div class=\"row\" style=\"height:20%\">\n    <div>\n      <h1 class=\"phone-h\">Call Centar<span><i class=\"fas fa-phone\"></i></span></h1>\n      \n      </div>\n\n\n</div>\n<div class=\"row row-items \">\n<div class=\"col\">\n\n  <div class=\"col-items col-lica\">\n<div class=\"row\">\n  <div class=\"col lica\">\n      <div class=\"fizicka\"><h1 class=\"footer-h\">Fizicka Lica</h1>\n        <p class=\"footer-para\">011 44 14 000</p>\n        </div>\n        <div  ><h1 class=\"footer-h\">Pravna Lica</h1>\n          <p class=\"footer-para\">011 44 14 000</p>\n          </div>\n  </div>\n  <div class=\"col-pozivi\">\n\n      <h1 class=\"footer-h\">Pozivi sa mobilne mreže</h1>\n      <p class=\"footer-para\">066 6 67 67 67</p>\n  </div>\n</div>\n\n  </div>\n  <div class=\"col-items col-news\">\n      <div>\n          <p class=\"prijava-p\">Prijavite se na Newsletter</p>\n        </div>\n        <div>\n          <input class=\"input\" placeholder=\"insert your mail\"><button class=\"button\">Prijavi se</button>\n          </div>\n        </div>\n        <div class=\"col-items\">\n  \n  \n<div>\n    <h1 class=\"footer-h\">Radno vreme Call Centra</h1>\n    <ul class=\"footer-ul\">\n      <li>Ponedeljak - Petak: od 08h do 20h</li>\n      <li>Subota: od 09h do 17h</li>\n      <li>Nedelja: neradni dan</li>\n      </ul>\n  </div>\n\n          </div>\n          \n  </div>\n\n\n\n\n</div>\n\n\n \n  \n</footer>\n\n\n"

/***/ }),

/***/ "./src/app/components/footer/footer.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/footer/footer.component.ts ***!
  \*******************************************************/
/*! exports provided: FooterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FooterComponent", function() { return FooterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__(/*! ./footer.component.html */ "./src/app/components/footer/footer.component.html"),
            styles: [__webpack_require__(/*! ./footer.component.css */ "./src/app/components/footer/footer.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/header/header.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\nheader{\r\n  \r\n    background: white;\r\n\r\n   \r\n    color: white;\r\n \r\n    \r\n}\r\n\r\n.flex{\r\n    display: flex;\r\n    height:20px;\r\n}\r\n\r\nul{\r\ndisplay: flex;\r\nheight: 100%;\r\nwidth: 100%;\r\nlist-style: none;\r\n    \r\n}\r\n\r\nul li{\r\n   \r\n   \r\n    height: 100%;\r\n    width: 100%;\r\n  \r\n}\r\n\r\n.logo{\r\n \r\n    /* border: 2px solid black; */\r\n    height: 100%;\r\n    display: flex;\r\n    margin: 1rem 0.5rem;\r\n    \r\n}\r\n\r\n.logo-text1{\r\n   \r\n    color: rgb(197, 188, 188);\r\n}\r\n\r\n.logo-text2{\r\n    color: red;\r\n    font-size: 30px;\r\n    margin-left: -3px;\r\n}\r\n\r\n.logo-image{\r\n    color: black;\r\n}\r\n\r\n.img{\r\n    height: 70px;\r\n    width: 70px;\r\n    position: relative;\r\n    bottom: 16px;\r\n}\r\n\r\n.img img{\r\n    height: 100%;\r\n    width: 90%;\r\n}\r\n\r\n@media screen and (max-width:490px){\r\n\r\n    ul{\r\n        width: 100%;\r\n        height: 100%;\r\n        list-style: none;\r\n    }\r\n.logo{\r\n\r\n    margin:1rem 0.3rem;\r\n    position: relative;\r\n    right: 1rem;\r\n    top: 0.7rem;\r\n\r\n   \r\n\r\n}\r\n\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTs7SUFFSSxpQkFBaUI7OztJQUdqQixZQUFZOzs7QUFHaEI7O0FBRUE7SUFDSSxhQUFhO0lBQ2IsV0FBVztBQUNmOztBQUNBO0FBQ0EsYUFBYTtBQUNiLFlBQVk7QUFDWixXQUFXO0FBQ1gsZ0JBQWdCOztBQUVoQjs7QUFDQTs7O0lBR0ksWUFBWTtJQUNaLFdBQVc7O0FBRWY7O0FBR0E7O0lBRUksNkJBQTZCO0lBQzdCLFlBQVk7SUFDWixhQUFhO0lBQ2IsbUJBQW1COztBQUV2Qjs7QUFFQTs7SUFFSSx5QkFBeUI7QUFDN0I7O0FBRUE7SUFDSSxVQUFVO0lBQ1YsZUFBZTtJQUNmLGlCQUFpQjtBQUNyQjs7QUFDQTtJQUNJLFlBQVk7QUFDaEI7O0FBQ0E7SUFDSSxZQUFZO0lBQ1osV0FBVztJQUNYLGtCQUFrQjtJQUNsQixZQUFZO0FBQ2hCOztBQUNBO0lBQ0ksWUFBWTtJQUNaLFVBQVU7QUFDZDs7QUFHQTs7SUFFSTtRQUNJLFdBQVc7UUFDWCxZQUFZO1FBQ1osZ0JBQWdCO0lBQ3BCO0FBQ0o7O0lBRUksa0JBQWtCO0lBQ2xCLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsV0FBVzs7OztBQUlmOztBQUVBIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuXHJcbmhlYWRlcntcclxuICBcclxuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xyXG5cclxuICAgXHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiBcclxuICAgIFxyXG59XHJcblxyXG4uZmxleHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBoZWlnaHQ6MjBweDtcclxufVxyXG51bHtcclxuZGlzcGxheTogZmxleDtcclxuaGVpZ2h0OiAxMDAlO1xyXG53aWR0aDogMTAwJTtcclxubGlzdC1zdHlsZTogbm9uZTtcclxuICAgIFxyXG59XHJcbnVsIGxpe1xyXG4gICBcclxuICAgXHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICBcclxufVxyXG5cclxuXHJcbi5sb2dve1xyXG4gXHJcbiAgICAvKiBib3JkZXI6IDJweCBzb2xpZCBibGFjazsgKi9cclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBtYXJnaW46IDFyZW0gMC41cmVtO1xyXG4gICAgXHJcbn1cclxuXHJcbi5sb2dvLXRleHQxe1xyXG4gICBcclxuICAgIGNvbG9yOiByZ2IoMTk3LCAxODgsIDE4OCk7XHJcbn1cclxuXHJcbi5sb2dvLXRleHQye1xyXG4gICAgY29sb3I6IHJlZDtcclxuICAgIGZvbnQtc2l6ZTogMzBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAtM3B4O1xyXG59XHJcbi5sb2dvLWltYWdle1xyXG4gICAgY29sb3I6IGJsYWNrO1xyXG59XHJcbi5pbWd7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICB3aWR0aDogNzBweDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIGJvdHRvbTogMTZweDtcclxufVxyXG4uaW1nIGltZ3tcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHdpZHRoOiA5MCU7XHJcbn1cclxuXHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjQ5MHB4KXtcclxuXHJcbiAgICB1bHtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIH1cclxuLmxvZ297XHJcblxyXG4gICAgbWFyZ2luOjFyZW0gMC4zcmVtO1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgcmlnaHQ6IDFyZW07XHJcbiAgICB0b3A6IDAuN3JlbTtcclxuXHJcbiAgIFxyXG5cclxufVxyXG5cclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/header/header.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<header>\r\n\r\n\r\n<div class=\"flex\" >\r\n    <ul *ngFor=\"let color of colors\">\r\n        <li [style.background]=\"color\"></li>\r\n    </ul>\r\n</div>\r\n\r\n<div class=\"logo\">\r\n<div class=\"logo-image\">\r\n<div class=\"img\"><img src=\"../../../assets/logo.jpg\"></div>\r\n\r\n\r\n</div>\r\n<div class=\"logo-text-waper\">\r\n<div class=\"logo-text2\">Bogi Shop</div>\r\n<div class=\"logo-text1\">kupi kvalitento</div>\r\n</div>\r\n</div>\r\n\r\n</header>"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_header_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/header.service */ "./src/app/services/header.service.ts");



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(headerservice) {
        this.headerservice = headerservice;
    }
    HeaderComponent.prototype.ngOnInit = function () {
        this.colors = this.headerservice.getColors();
    };
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! ./header.component.html */ "./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.css */ "./src/app/components/header/header.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_header_service__WEBPACK_IMPORTED_MODULE_2__["HeaderService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/home-items/home-items.component.css":
/*!****************************************************************!*\
  !*** ./src/app/components/home-items/home-items.component.css ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "*{\r\n    box-sizing: border-box;\r\n}\r\n\r\n\r\n/* items boxes */\r\n\r\n\r\n.item-img{\r\n    height: 130px;\r\n    background-size: cover;\r\n   width: 60%;\r\n   margin: 0 auto;\r\n\r\n\r\n}\r\n\r\n\r\n.home-items{\r\n    display: flex;\r\n    flex-wrap: wrap;\r\n    justify-content: center;\r\n    margin: 2rem 3rem 2rem 3rem;\r\n\r\n}\r\n\r\n\r\n.wraper{\r\n   \r\n    margin: 0 auto;\r\n    border: 1px solid rgb(185, 199, 199);;\r\n   \r\n    margin: 0.5rem 0rem;\r\n  \r\n   \r\n}\r\n\r\n\r\n.items{\r\n    width: 25%;\r\n    text-align: center;\r\n\r\n    font-family: fantasy;\r\n\r\n    position: relative;\r\n    z-index: 3;\r\n  \r\n \r\n   \r\n  \r\n   \r\n   \r\n\r\n}\r\n\r\n\r\n.btn{\r\n   \r\n    color: white;\r\n    margin: 1rem 0;\r\n    font-family: fantasy;\r\n    padding: 0.5rem 0;\r\n    text-transform: uppercase;\r\n    outline: none;\r\n    border: none;\r\n    width: 90%;\r\n    \r\n\r\n}\r\n\r\n\r\n.item-p{\r\n\r\n    width: 100%;\r\n    margin: 1rem 0;\r\n    padding: 0.3rem 0;\r\n}\r\n\r\n\r\n.price{\r\n    background: rgb(171, 206, 206);\r\n    width: 90%;\r\n    margin:  0 auto;\r\n}\r\n\r\n\r\n.preporuka{\r\n\r\n    color: rgb(168, 161, 161);\r\n  margin: 0;\r\n\r\n    font-size: 23px;\r\n    margin-left: 3rem;\r\n    margin-top: 1rem;\r\n\r\n    \r\n\r\n}\r\n\r\n\r\n@media screen and (max-width:750px){\r\n\r\n    .home-items{\r\n        flex-direction: row;\r\n        margin: 0 auto;\r\n        \r\n    \r\n    }\r\n    .items{\r\n        margin: 0 auto;\r\n        width: 40%;\r\n    }\r\n}\r\n\r\n\r\n@media screen and (max-width:490px){\r\n  \r\n\r\n    .home-items{\r\n        flex-direction: column;\r\n    }\r\n\r\n    .items{\r\n        margin: 0 auto;\r\n        width: 90%;\r\n    }\r\n    \r\n    .preporuka{\r\n        margin-left: 1rem;\r\n    }\r\n}\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ob21lLWl0ZW1zL2hvbWUtaXRlbXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLHNCQUFzQjtBQUMxQjs7O0FBR0EsZ0JBQWdCOzs7QUFHaEI7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0dBQ3ZCLFVBQVU7R0FDVixjQUFjOzs7QUFHakI7OztBQUdBO0lBQ0ksYUFBYTtJQUNiLGVBQWU7SUFDZix1QkFBdUI7SUFDdkIsMkJBQTJCOztBQUUvQjs7O0FBS0E7O0lBRUksY0FBYztJQUNkLG9DQUFvQzs7SUFFcEMsbUJBQW1COzs7QUFHdkI7OztBQUtBO0lBQ0ksVUFBVTtJQUNWLGtCQUFrQjs7SUFFbEIsb0JBQW9COztJQUVwQixrQkFBa0I7SUFDbEIsVUFBVTs7Ozs7Ozs7QUFRZDs7O0FBRUE7O0lBRUksWUFBWTtJQUNaLGNBQWM7SUFDZCxvQkFBb0I7SUFDcEIsaUJBQWlCO0lBQ2pCLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2IsWUFBWTtJQUNaLFVBQVU7OztBQUdkOzs7QUFDQTs7SUFFSSxXQUFXO0lBQ1gsY0FBYztJQUNkLGlCQUFpQjtBQUNyQjs7O0FBR0E7SUFDSSw4QkFBOEI7SUFDOUIsVUFBVTtJQUNWLGVBQWU7QUFDbkI7OztBQUdBOztJQUVJLHlCQUF5QjtFQUMzQixTQUFTOztJQUVQLGVBQWU7SUFDZixpQkFBaUI7SUFDakIsZ0JBQWdCOzs7O0FBSXBCOzs7QUFJQTs7SUFFSTtRQUNJLG1CQUFtQjtRQUNuQixjQUFjOzs7SUFHbEI7SUFDQTtRQUNJLGNBQWM7UUFDZCxVQUFVO0lBQ2Q7QUFDSjs7O0FBQ0E7OztJQUdJO1FBQ0ksc0JBQXNCO0lBQzFCOztJQUVBO1FBQ0ksY0FBYztRQUNkLFVBQVU7SUFDZDs7SUFFQTtRQUNJLGlCQUFpQjtJQUNyQjtBQUNKIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9ob21lLWl0ZW1zL2hvbWUtaXRlbXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIip7XHJcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG59XHJcblxyXG5cclxuLyogaXRlbXMgYm94ZXMgKi9cclxuXHJcblxyXG4uaXRlbS1pbWd7XHJcbiAgICBoZWlnaHQ6IDEzMHB4O1xyXG4gICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAgd2lkdGg6IDYwJTtcclxuICAgbWFyZ2luOiAwIGF1dG87XHJcblxyXG5cclxufVxyXG5cclxuXHJcbi5ob21lLWl0ZW1ze1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgbWFyZ2luOiAycmVtIDNyZW0gMnJlbSAzcmVtO1xyXG5cclxufVxyXG5cclxuXHJcblxyXG5cclxuLndyYXBlcntcclxuICAgXHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYigxODUsIDE5OSwgMTk5KTs7XHJcbiAgIFxyXG4gICAgbWFyZ2luOiAwLjVyZW0gMHJlbTtcclxuICBcclxuICAgXHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5pdGVtc3tcclxuICAgIHdpZHRoOiAyNSU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgZm9udC1mYW1pbHk6IGZhbnRhc3k7XHJcblxyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgei1pbmRleDogMztcclxuICBcclxuIFxyXG4gICBcclxuICBcclxuICAgXHJcbiAgIFxyXG5cclxufVxyXG5cclxuLmJ0bntcclxuICAgXHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW46IDFyZW0gMDtcclxuICAgIGZvbnQtZmFtaWx5OiBmYW50YXN5O1xyXG4gICAgcGFkZGluZzogMC41cmVtIDA7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIHdpZHRoOiA5MCU7XHJcbiAgICBcclxuXHJcbn1cclxuLml0ZW0tcHtcclxuXHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbjogMXJlbSAwO1xyXG4gICAgcGFkZGluZzogMC4zcmVtIDA7XHJcbn1cclxuXHJcblxyXG4ucHJpY2V7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMTcxLCAyMDYsIDIwNik7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgbWFyZ2luOiAgMCBhdXRvO1xyXG59XHJcblxyXG5cclxuLnByZXBvcnVrYXtcclxuXHJcbiAgICBjb2xvcjogcmdiKDE2OCwgMTYxLCAxNjEpO1xyXG4gIG1hcmdpbjogMDtcclxuXHJcbiAgICBmb250LXNpemU6IDIzcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogM3JlbTtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcblxyXG4gICAgXHJcblxyXG59XHJcblxyXG5cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6NzUwcHgpe1xyXG5cclxuICAgIC5ob21lLWl0ZW1ze1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgXHJcbiAgICBcclxuICAgIH1cclxuICAgIC5pdGVtc3tcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICB3aWR0aDogNDAlO1xyXG4gICAgfVxyXG59XHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6NDkwcHgpe1xyXG4gIFxyXG5cclxuICAgIC5ob21lLWl0ZW1ze1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICB9XHJcblxyXG4gICAgLml0ZW1ze1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiA5MCU7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5wcmVwb3J1a2F7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDFyZW07XHJcbiAgICB9XHJcbn1cclxuXHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/home-items/home-items.component.html":
/*!*****************************************************************!*\
  !*** ./src/app/components/home-items/home-items.component.html ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n <div class=\"loader\" *ngIf=\"loading\">Loading...</div>\r\n\r\n<div class=\"home-items\">\r\n       \r\n\r\n\r\n    <div class=\"items\" *ngFor=\"let item of items\">\r\n     <div class=\"wraper\">\r\n           \r\n                       <p class=\"item-p\">{{item.name |uppercase}}</p>\r\n   \r\n                       <div  [class]=\"item.class\" ><img [src]=\"item.bck\" width=\"100%\" height=\"100%\"></div>\r\n   \r\n                       <p class=\"item-p price\">Cena:{{item.price}}RSD</p>\r\n   \r\n              \r\n                       <button class=\"btn detail\"style=\"background:orange\" (click)=\"Details(item)\" [routerLink]=\"['/item', item.rout,item.rand,item.name]\"><span class=\"details-icon\"><i class=\"fas fa-pencil-alt\"></i></span>Detalji...</button>\r\n                     \r\n                       <button (click)=\"addToCart(item)\" class=\"btn\" [id]=\"item.id\" ><span  class=\"cart-icon\"><i class=\"fas fa-shopping-cart\"></i></span>Dodaj u kantu</button>\r\n           \r\n   </div>        \r\n   </div>\r\n   </div>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/components/home-items/home-items.component.ts":
/*!***************************************************************!*\
  !*** ./src/app/components/home-items/home-items.component.ts ***!
  \***************************************************************/
/*! exports provided: HomeItemsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeItemsComponent", function() { return HomeItemsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/cart.service */ "./src/app/services/cart.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);





var HomeItemsComponent = /** @class */ (function () {
    function HomeItemsComponent(cartService, rout) {
        this.cartService = cartService;
        this.rout = rout;
        this.toogle = true;
        this.toTal = 0;
        this.item = [];
        this.loading = true;
        this.toogleDeails = true;
    }
    HomeItemsComponent.prototype.ngOnInit = function () {
        var _this = this;
        axios__WEBPACK_IMPORTED_MODULE_4___default.a.get("https://burger-builder-1232d.firebaseio.com/home-items.json")
            .then(function (data) {
            _this.items = data.data;
            _this.loading = false;
        });
        // get all items
        // let data=this.cartService.getData()
        // Get data from firebase
        this.cartService.data.subscribe(function (data) {
            _this.items = data;
        });
    };
    HomeItemsComponent.prototype.ngOnChanges = function () {
        console.log("changes");
    };
    // Add items to a Cart...
    HomeItemsComponent.prototype.addToCart = function (item) {
        this.cartService.add(item);
        this.cartService.openModal();
    };
    HomeItemsComponent.prototype.Details = function (item) {
        this.cartService.Details(item);
        this.cartService.getHomeVal(item);
    };
    HomeItemsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home-items',
            template: __webpack_require__(/*! ./home-items.component.html */ "./src/app/components/home-items/home-items.component.html"),
            styles: [__webpack_require__(/*! ./home-items.component.css */ "./src/app/components/home-items/home-items.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], HomeItemsComponent);
    return HomeItemsComponent;
}());



/***/ }),

/***/ "./src/app/components/home/home.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/home/home.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".modal{\r\n\tposition: fixed;\r\n\ttext-align: center;\r\n\tpadding: 0.3rem 0;\r\n\tcolor: green;\r\n\twidth: 30%;\r\n\ttop:10px;\r\n\tbackground:wheat;\r\nleft: 35%;\r\nz-index: 9999;\r\n\t\r\n}\r\n.item-img{\r\n    height: 130px;\r\n    background-size: cover;\r\n   width: 60%;\r\n   margin: 0 auto;\r\n\r\n\r\n}\r\n.home-items{\r\n    display: flex;\r\n    flex-wrap: wrap;\r\n    justify-content: center;\r\n    margin: 2rem 3rem 2rem 3rem;\r\n\r\n}\r\n.wraper{\r\n   \r\n    margin: 0 auto;\r\n    border: 1px solid rgb(185, 199, 199);;\r\n   \r\n    margin: 0.5rem 0rem;\r\n  \r\n   \r\n}\r\n.items{\r\n    width: 25%;\r\n    text-align: center;\r\n\r\n    font-family: fantasy;\r\n\r\n    position: relative;\r\n    z-index: 3;\r\n  \r\n \r\n   \r\n  \r\n   \r\n   \r\n\r\n}\r\n.btn{\r\n   \r\n    color: white;\r\n    margin: 1rem 0;\r\n    font-family: fantasy;\r\n    padding: 0.5rem 0;\r\n    text-transform: uppercase;\r\n    outline: none;\r\n    border: none;\r\n    width: 90%;\r\n    \r\n\r\n}\r\n.item-p{\r\n\r\n    width: 100%;\r\n    margin: 1rem 0;\r\n    padding: 0.3rem 0;\r\n}\r\n.price{\r\n    background: rgb(171, 206, 206);\r\n    width: 90%;\r\n    margin:  0 auto;\r\n}\r\n.preporuka{\r\n\r\n    color: rgb(168, 161, 161);\r\n  margin: 0;\r\n\r\n    font-size: 23px;\r\n    margin-left: 3rem;\r\n    margin-top: 1rem;\r\n\r\n    \r\n\r\n}\r\n@media screen and (max-width:750px){\r\n\r\n    .home-items{\r\n        flex-direction: row;\r\n        margin: 0 auto;\r\n        \r\n    \r\n    }\r\n    .items{\r\n        margin: 0 auto;\r\n        width: 40%;\r\n    }\r\n}\r\n@media screen and (max-width:490px){\r\n  \r\n\r\n    .home-items{\r\n        flex-direction: column;\r\n    }\r\n\r\n    .items{\r\n        margin: 0 auto;\r\n        width: 90%;\r\n    }\r\n    \r\n    .preporuka{\r\n        margin-left: 1rem;\r\n    }\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9ob21lL2hvbWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtDQUNDLGVBQWU7Q0FDZixrQkFBa0I7Q0FDbEIsaUJBQWlCO0NBQ2pCLFlBQVk7Q0FDWixVQUFVO0NBQ1YsUUFBUTtDQUNSLGdCQUFnQjtBQUNqQixTQUFTO0FBQ1QsYUFBYTs7QUFFYjtBQUNBO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtHQUN2QixVQUFVO0dBQ1YsY0FBYzs7O0FBR2pCO0FBR0E7SUFDSSxhQUFhO0lBQ2IsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QiwyQkFBMkI7O0FBRS9CO0FBS0E7O0lBRUksY0FBYztJQUNkLG9DQUFvQzs7SUFFcEMsbUJBQW1COzs7QUFHdkI7QUFLQTtJQUNJLFVBQVU7SUFDVixrQkFBa0I7O0lBRWxCLG9CQUFvQjs7SUFFcEIsa0JBQWtCO0lBQ2xCLFVBQVU7Ozs7Ozs7O0FBUWQ7QUFFQTs7SUFFSSxZQUFZO0lBQ1osY0FBYztJQUNkLG9CQUFvQjtJQUNwQixpQkFBaUI7SUFDakIseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYixZQUFZO0lBQ1osVUFBVTs7O0FBR2Q7QUFDQTs7SUFFSSxXQUFXO0lBQ1gsY0FBYztJQUNkLGlCQUFpQjtBQUNyQjtBQUdBO0lBQ0ksOEJBQThCO0lBQzlCLFVBQVU7SUFDVixlQUFlO0FBQ25CO0FBR0E7O0lBRUkseUJBQXlCO0VBQzNCLFNBQVM7O0lBRVAsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixnQkFBZ0I7Ozs7QUFJcEI7QUFJQTs7SUFFSTtRQUNJLG1CQUFtQjtRQUNuQixjQUFjOzs7SUFHbEI7SUFDQTtRQUNJLGNBQWM7UUFDZCxVQUFVO0lBQ2Q7QUFDSjtBQUNBOzs7SUFHSTtRQUNJLHNCQUFzQjtJQUMxQjs7SUFFQTtRQUNJLGNBQWM7UUFDZCxVQUFVO0lBQ2Q7O0lBRUE7UUFDSSxpQkFBaUI7SUFDckI7QUFDSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaG9tZS9ob21lLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubW9kYWx7XHJcblx0cG9zaXRpb246IGZpeGVkO1xyXG5cdHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHRwYWRkaW5nOiAwLjNyZW0gMDtcclxuXHRjb2xvcjogZ3JlZW47XHJcblx0d2lkdGg6IDMwJTtcclxuXHR0b3A6MTBweDtcclxuXHRiYWNrZ3JvdW5kOndoZWF0O1xyXG5sZWZ0OiAzNSU7XHJcbnotaW5kZXg6IDk5OTk7XHJcblx0XHJcbn1cclxuLml0ZW0taW1ne1xyXG4gICAgaGVpZ2h0OiAxMzBweDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgIHdpZHRoOiA2MCU7XHJcbiAgIG1hcmdpbjogMCBhdXRvO1xyXG5cclxuXHJcbn1cclxuXHJcblxyXG4uaG9tZS1pdGVtc3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMnJlbSAzcmVtIDJyZW0gM3JlbTtcclxuXHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi53cmFwZXJ7XHJcbiAgIFxyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMTg1LCAxOTksIDE5OSk7O1xyXG4gICBcclxuICAgIG1hcmdpbjogMC41cmVtIDByZW07XHJcbiAgXHJcbiAgIFxyXG59XHJcblxyXG5cclxuXHJcblxyXG4uaXRlbXN7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIGZvbnQtZmFtaWx5OiBmYW50YXN5O1xyXG5cclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDM7XHJcbiAgXHJcbiBcclxuICAgXHJcbiAgXHJcbiAgIFxyXG4gICBcclxuXHJcbn1cclxuXHJcbi5idG57XHJcbiAgIFxyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgbWFyZ2luOiAxcmVtIDA7XHJcbiAgICBmb250LWZhbWlseTogZmFudGFzeTtcclxuICAgIHBhZGRpbmc6IDAuNXJlbSAwO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgXHJcblxyXG59XHJcbi5pdGVtLXB7XHJcblxyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDFyZW0gMDtcclxuICAgIHBhZGRpbmc6IDAuM3JlbSAwO1xyXG59XHJcblxyXG5cclxuLnByaWNle1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDE3MSwgMjA2LCAyMDYpO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIG1hcmdpbjogIDAgYXV0bztcclxufVxyXG5cclxuXHJcbi5wcmVwb3J1a2F7XHJcblxyXG4gICAgY29sb3I6IHJnYigxNjgsIDE2MSwgMTYxKTtcclxuICBtYXJnaW46IDA7XHJcblxyXG4gICAgZm9udC1zaXplOiAyM3B4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDNyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG5cclxuICAgIFxyXG5cclxufVxyXG5cclxuXHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjc1MHB4KXtcclxuXHJcbiAgICAuaG9tZS1pdGVtc3tcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIFxyXG4gICAgXHJcbiAgICB9XHJcbiAgICAuaXRlbXN7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgd2lkdGg6IDQwJTtcclxuICAgIH1cclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjQ5MHB4KXtcclxuICBcclxuXHJcbiAgICAuaG9tZS1pdGVtc3tcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgfVxyXG5cclxuICAgIC5pdGVtc3tcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAucHJlcG9ydWthe1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxcmVtO1xyXG4gICAgfVxyXG59Il19 */"

/***/ }),

/***/ "./src/app/components/home/home.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/home/home.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n \n<app-slider></app-slider>\n<app-home-items></app-home-items>\n\n<div class=\"modal\" *ngIf=\"show\">Dodato u kantu</div>\n\n<div class=\"home-items\">\n       \n\n\n        <div class=\"items\" *ngFor=\"let item of items\">\n         <div class=\"wraper\">\n               \n                           <p class=\"item-p\">{{item.name |uppercase}}</p>\n       \n                           <div  [class]=\"item.class\" ><img [src]=\"item.bck\" width=\"100%\" height=\"100%\"></div>\n       \n                        \n       </div>        \n       </div>\n       </div>"

/***/ }),

/***/ "./src/app/components/home/home.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/home/home.component.ts ***!
  \***************************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/cart.service */ "./src/app/services/cart.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);





var HomeComponent = /** @class */ (function () {
    function HomeComponent(cartService, router) {
        this.cartService = cartService;
        this.router = router;
    }
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        axios__WEBPACK_IMPORTED_MODULE_4___default.a.get("https://burger-builder-1232d.firebaseio.com/shopOrders.json")
            .then(function (data) {
            var d = data.data;
            var arr = [];
            for (var key in d) {
                arr.push(d[key].items);
            }
            _this.items = arr[0];
        });
        // Show modal when user add a item..
        this.cartService.Modal.subscribe(function (modal) {
            _this.show = modal;
        });
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/components/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/components/home/home.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/components/item/item.component.css":
/*!****************************************************!*\
  !*** ./src/app/components/item/item.component.css ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".item-inner{\r\n    display: flex;\r\n    flex-direction: row;\r\n    align-items: center;\r\n    flex-wrap: wrap;\r\n    align-items: center;\r\n    justify-content: center;\r\n}\r\n\r\n.btn{\r\n\r\n    width: 100%;\r\n    border: none;\r\n    padding: 0.5rem 1rem;\r\n\r\n}\r\n\r\n.price{\r\n    margin: 0.5rem;\r\n}\r\n\r\n.image{\r\n    text-align: center;\r\n    margin: 0.5rem;\r\n}\r\n\r\n.item-name{\r\n    margin: 0.5rem; \r\n}\r\n\r\np{\r\n    text-align: center;\r\n}\r\n\r\nh1{\r\n    text-align: center;\r\n\r\n}\r\n\r\n.item{\r\n    margin: 1rem 0; \r\n\r\n    width: 40%;\r\n    border: 1px solid lightgray;\r\n    margin: 0.5rem 1rem;\r\n}\r\n\r\n.div1{\r\n    display: flex;\r\n}\r\n\r\n.cart{\r\n    padding: 1rem 0.5rem;\r\n}\r\n\r\ntable {\r\n    border-collapse: collapse;\r\n    border-spacing: 0;\r\n    width: 100%;\r\n    border: 1px solid #ddd;\r\n    margin: 2rem 0;\r\n  }\r\n\r\ntd,tr {\r\n  padding: 0.7rem 0 0.7rem 0.3rem; \r\n  }\r\n\r\ntd{\r\n      border: 1px solid gray;\r\n  }\r\n\r\ntr:nth-child(even) {\r\n    background-color: #f2f2f2\r\n  }\r\n\r\n.info{\r\n    width: 100%;\r\n    display: flex;\r\n    \r\n\r\n}\r\n\r\n.div1,.div2{\r\n    /* display: flex;\r\n    flex-direction: column; */\r\n}\r\n\r\n.wraper,.wrap2{\r\n    display: flex;\r\n    flex-direction: column;\r\n}\r\n\r\n.flex{\r\n    display: flex;\r\n}\r\n\r\n.flex1{\r\n    display: flex;\r\n    flex-direction: column;\r\n    width: 40%;\r\n    border: 1px solid gray;\r\n}\r\n\r\n.flex2{\r\n    display: flex;\r\n    \r\n    flex-direction: column;\r\n    border: 1px solid gray;\r\n    width: 60%;\r\n}\r\n\r\nul{\r\n    list-style: none;\r\n}\r\n\r\n.modal{\r\n\tposition: fixed;\r\n\ttext-align: center;\r\n\tpadding: 0.3rem 0;\r\n\tcolor: green;\r\n\twidth: 30%;\r\n\ttop:10px;\r\n\tbackground:wheat;\r\nleft: 35%;\r\nz-index: 9999;\r\n\t\r\n}\r\n\r\n@media screen and (max-width:750px){\r\n\r\n  \r\n\r\n    .item-inner{\r\n        flex-direction: column;\r\n    \r\n    }\r\n    .item{\r\n       width: 50%;margin: 1rem auto;\r\n\r\n    }\r\n\r\n\r\n\r\n\r\n}\r\n\r\n@media screen and (max-width:490px){\r\n  \r\n.item-inner{\r\n    flex-direction: column;\r\n\r\n}\r\n.item{\r\n    width: 100%;\r\n}\r\n  \r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9pdGVtL2l0ZW0uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsdUJBQXVCO0FBQzNCOztBQUVBOztJQUVJLFdBQVc7SUFDWCxZQUFZO0lBQ1osb0JBQW9COztBQUV4Qjs7QUFDQTtJQUNJLGNBQWM7QUFDbEI7O0FBQ0E7SUFDSSxrQkFBa0I7SUFDbEIsY0FBYztBQUNsQjs7QUFFQTtJQUNJLGNBQWM7QUFDbEI7O0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7O0FBQ0E7SUFDSSxrQkFBa0I7O0FBRXRCOztBQUNBO0lBQ0ksY0FBYzs7SUFFZCxVQUFVO0lBQ1YsMkJBQTJCO0lBQzNCLG1CQUFtQjtBQUN2Qjs7QUFFQTtJQUNJLGFBQWE7QUFDakI7O0FBQ0E7SUFDSSxvQkFBb0I7QUFDeEI7O0FBRUE7SUFDSSx5QkFBeUI7SUFDekIsaUJBQWlCO0lBQ2pCLFdBQVc7SUFDWCxzQkFBc0I7SUFDdEIsY0FBYztFQUNoQjs7QUFFQTtFQUNBLCtCQUErQjtFQUMvQjs7QUFDQTtNQUNJLHNCQUFzQjtFQUMxQjs7QUFFQTtJQUNFO0VBQ0Y7O0FBR0Y7SUFDSSxXQUFXO0lBQ1gsYUFBYTs7O0FBR2pCOztBQUNBO0lBQ0k7NkJBQ3lCO0FBQzdCOztBQUNBO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtBQUMxQjs7QUFJQTtJQUNJLGFBQWE7QUFDakI7O0FBQ0E7SUFDSSxhQUFhO0lBQ2Isc0JBQXNCO0lBQ3RCLFVBQVU7SUFDVixzQkFBc0I7QUFDMUI7O0FBQ0E7SUFDSSxhQUFhOztJQUViLHNCQUFzQjtJQUN0QixzQkFBc0I7SUFDdEIsVUFBVTtBQUNkOztBQUVBO0lBQ0ksZ0JBQWdCO0FBQ3BCOztBQUVBO0NBQ0MsZUFBZTtDQUNmLGtCQUFrQjtDQUNsQixpQkFBaUI7Q0FDakIsWUFBWTtDQUNaLFVBQVU7Q0FDVixRQUFRO0NBQ1IsZ0JBQWdCO0FBQ2pCLFNBQVM7QUFDVCxhQUFhOztBQUViOztBQUVBOzs7O0lBSUk7UUFDSSxzQkFBc0I7O0lBRTFCO0lBQ0E7T0FDRyxVQUFVLENBQUMsaUJBQWlCOztJQUUvQjs7Ozs7QUFLSjs7QUFJQTs7QUFFQTtJQUNJLHNCQUFzQjs7QUFFMUI7QUFDQTtJQUNJLFdBQVc7QUFDZjs7QUFFQSIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvaXRlbS9pdGVtLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaXRlbS1pbm5lcntcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxufVxyXG5cclxuLmJ0bntcclxuXHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIHBhZGRpbmc6IDAuNXJlbSAxcmVtO1xyXG5cclxufVxyXG4ucHJpY2V7XHJcbiAgICBtYXJnaW46IDAuNXJlbTtcclxufVxyXG4uaW1hZ2V7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDAuNXJlbTtcclxufVxyXG5cclxuLml0ZW0tbmFtZXtcclxuICAgIG1hcmdpbjogMC41cmVtOyBcclxufVxyXG5we1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcbmgxe1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxufVxyXG4uaXRlbXtcclxuICAgIG1hcmdpbjogMXJlbSAwOyBcclxuXHJcbiAgICB3aWR0aDogNDAlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgbGlnaHRncmF5O1xyXG4gICAgbWFyZ2luOiAwLjVyZW0gMXJlbTtcclxufVxyXG5cclxuLmRpdjF7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcbi5jYXJ0e1xyXG4gICAgcGFkZGluZzogMXJlbSAwLjVyZW07XHJcbn1cclxuXHJcbnRhYmxlIHtcclxuICAgIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbiAgICBib3JkZXItc3BhY2luZzogMDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2RkZDtcclxuICAgIG1hcmdpbjogMnJlbSAwO1xyXG4gIH1cclxuICBcclxuICB0ZCx0ciB7XHJcbiAgcGFkZGluZzogMC43cmVtIDAgMC43cmVtIDAuM3JlbTsgXHJcbiAgfVxyXG4gIHRke1xyXG4gICAgICBib3JkZXI6IDFweCBzb2xpZCBncmF5O1xyXG4gIH1cclxuICBcclxuICB0cjpudGgtY2hpbGQoZXZlbikge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMlxyXG4gIH1cclxuXHJcblxyXG4uaW5mb3tcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIFxyXG5cclxufVxyXG4uZGl2MSwuZGl2MntcclxuICAgIC8qIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uOyAqL1xyXG59XHJcbi53cmFwZXIsLndyYXAye1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbn1cclxuXHJcblxyXG5cclxuLmZsZXh7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcbi5mbGV4MXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgd2lkdGg6IDQwJTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyYXk7XHJcbn1cclxuLmZsZXgye1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIFxyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIGdyYXk7XHJcbiAgICB3aWR0aDogNjAlO1xyXG59XHJcblxyXG51bHtcclxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbn1cclxuXHJcbi5tb2RhbHtcclxuXHRwb3NpdGlvbjogZml4ZWQ7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdHBhZGRpbmc6IDAuM3JlbSAwO1xyXG5cdGNvbG9yOiBncmVlbjtcclxuXHR3aWR0aDogMzAlO1xyXG5cdHRvcDoxMHB4O1xyXG5cdGJhY2tncm91bmQ6d2hlYXQ7XHJcbmxlZnQ6IDM1JTtcclxuei1pbmRleDogOTk5OTtcclxuXHRcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo3NTBweCl7XHJcblxyXG4gIFxyXG5cclxuICAgIC5pdGVtLWlubmVye1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBcclxuICAgIH1cclxuICAgIC5pdGVte1xyXG4gICAgICAgd2lkdGg6IDUwJTttYXJnaW46IDFyZW0gYXV0bztcclxuXHJcbiAgICB9XHJcblxyXG5cclxuXHJcblxyXG59XHJcblxyXG5cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6NDkwcHgpe1xyXG4gIFxyXG4uaXRlbS1pbm5lcntcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcblxyXG59XHJcbi5pdGVte1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuICBcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/components/item/item.component.html":
/*!*****************************************************!*\
  !*** ./src/app/components/item/item.component.html ***!
  \*****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n  \n        \n   <div class=\"modal\" *ngIf=\"show\">Dodato u kantu</div>\n        <div class=\"item-wraper\">\n            <div class=\"item-inner\">\n                <div class=\"item\">\n\n                    <p class=\"item-name\">{{item.name |uppercase}}</p>\n  \n                    <div class=\"image\" ><img *ngIf=\"item.bck\" src=\"{{item.bck}}\" class=\"cart-img\"></div>\n                  \n                 \n                </div>\n\n\n<div class=\"item cart\">\n    <p class=\"price\">Price: {{item.price}} RSD</p>\n    <button (click)=\"singleItem(item)\" class=\"btn\"  ><span  class=\"cart-icon\"><i class=\"fas fa-shopping-cart\"></i></span>addToCart</button>\n    \n\n</div>\n\n\n\n            </div>\n         \n        </div>\n\n\n           \n  <table >\n\n    <tr>\n        <td>\n                {{products[0]}}\n        </td>\n           \n         <td>\n                {{vrednosti[0]}}\n         </td>  \n    \n         \n        </tr>\n        \n    <tr>\n\n        <td>\n                {{products[1]}}\n        </td>\n           \n            <td>\n                    {{vrednosti[1]}}\n             </td>  \n           \n     \n         \n        </tr>\n   \n        \n    <tr>\n        <td>\n                {{products[2]}}\n        </td>\n           \n            <td>\n                    {{vrednosti[2]}}\n             </td>  \n     \n         \n        </tr>\n         <tr>\n        <td>\n                {{products[3]}}\n        </td>\n           \n            <td>\n                    {{vrednosti[3]}}\n             </td>  \n     \n         \n        </tr>\n         <tr>\n        <td>\n                {{products[4]}}\n        </td>\n           \n            <td>\n                    {{vrednosti[4]}}\n             </td>  \n     \n         \n        </tr>\n         <tr>\n        <td>\n                {{products[5]}}\n        </td>\n           \n            <td>\n                    {{vrednosti[5]}}\n             </td>  \n     \n         \n        </tr>\n         <tr>\n        <td>\n                {{products[6]}}\n        </td>\n           \n            <td>\n                    {{vrednosti[6]}}\n             </td>  \n     \n         \n        </tr>\n   \n   \n        \n  </table>\n             \n              \n  \n  \n        \n     \n\n\n  \n\n\n"

/***/ }),

/***/ "./src/app/components/item/item.component.ts":
/*!***************************************************!*\
  !*** ./src/app/components/item/item.component.ts ***!
  \***************************************************/
/*! exports provided: ItemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemComponent", function() { return ItemComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/cart.service */ "./src/app/services/cart.service.ts");
/* harmony import */ var _services_laptop_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/laptop.service */ "./src/app/services/laptop.service.ts");
/* harmony import */ var _services_tv_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/tv.service */ "./src/app/services/tv.service.ts");
/* harmony import */ var _services_mobile_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../services/mobile.service */ "./src/app/services/mobile.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var ItemComponent = /** @class */ (function () {
    function ItemComponent(cartService, lapService, tvService, mobService, router) {
        this.cartService = cartService;
        this.lapService = lapService;
        this.tvService = tvService;
        this.mobService = mobService;
        this.router = router;
        this.cartitem = [];
        this.products = [];
        this.vrednosti = [];
        this.show = false;
    }
    ItemComponent.prototype.ngOnInit = function () {
        // Show modal when user add item..
        var _this = this;
        this.cartService.Modal.subscribe(function (modal) {
            _this.show = modal;
        });
        // Items from Home service//
        this.getDetails();
        if (this.router.url.includes("laptop")) {
            this.getLap();
            this.getVal2();
        }
        if (this.router.url.includes("tv")) {
            this.getTV();
            this.getVal();
        }
        if (this.router.url.includes("mobilni")) {
            this.getMob();
            this.getVal1();
        }
        if (this.router.url.includes("home-rand")) {
            this.getHomeAll();
        }
    };
    // Add to cart from single item;
    ItemComponent.prototype.singleItem = function (item) {
        this.cartService.add(item);
        this.cartService.openModal();
    };
    // Get details for a single item 
    ItemComponent.prototype.getDetails = function () {
        var _this = this;
        this.cartService.singleItem.subscribe(function (data) {
            _this.item = data;
        });
    };
    ItemComponent.prototype.getTV = function () {
        var _this = this;
        this.tvService.tv.subscribe(function (data) {
            _this.products = data;
        });
    };
    ItemComponent.prototype.getLap = function () {
        var _this = this;
        this.lapService.lap.subscribe(function (data) {
            _this.products = data;
        });
    };
    ItemComponent.prototype.getMob = function () {
        var _this = this;
        this.mobService.mob.subscribe(function (data) {
            _this.products = data;
        });
    };
    ItemComponent.prototype.getVal = function () {
        var _this = this;
        this.tvService.tvs.subscribe(function (data) {
            _this.vrednosti = data.vrednosti;
        });
    };
    ItemComponent.prototype.getVal1 = function () {
        var _this = this;
        this.mobService.mobs.subscribe(function (data) {
            _this.vrednosti = data.vrednosti;
        });
    };
    ItemComponent.prototype.getVal2 = function () {
        var _this = this;
        this.lapService.laps.subscribe(function (data) {
            _this.vrednosti = data.vrednosti;
        });
    };
    ItemComponent.prototype.getHomeAll = function () {
        var _this = this;
        this.cartService.homes.subscribe(function (data) {
            _this.vrednosti = data;
        });
    };
    ItemComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-item',
            template: __webpack_require__(/*! ./item.component.html */ "./src/app/components/item/item.component.html"),
            styles: [__webpack_require__(/*! ./item.component.css */ "./src/app/components/item/item.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"],
            _services_laptop_service__WEBPACK_IMPORTED_MODULE_3__["Laptop"],
            _services_tv_service__WEBPACK_IMPORTED_MODULE_4__["TvService"],
            _services_mobile_service__WEBPACK_IMPORTED_MODULE_5__["MobileService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], ItemComponent);
    return ItemComponent;
}());



/***/ }),

/***/ "./src/app/components/laptop/laptop.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/laptop/laptop.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n/* items boxes */\r\n\r\n\r\n.item-img{\r\n    height: 130px;\r\n    background-size: cover;\r\n   width: 60%;\r\n   margin: 0 auto;\r\n\r\n\r\n}\r\n\r\n\r\n.home-items{\r\n    display: flex;\r\n    flex-wrap: wrap;\r\n    justify-content: center;\r\n    margin: 2rem 3rem 2rem 3rem;\r\n}\r\n\r\n\r\n.wraper{\r\n   \r\n    margin: 0 auto;\r\n    border: 1px solid rgb(185, 199, 199);;\r\n   \r\n    margin: 0.5rem 0rem;\r\n  \r\n   \r\n}\r\n\r\n\r\n.items{\r\n    width: 25%;\r\n    text-align: center;\r\n\r\n    font-family: fantasy;\r\n\r\n    position: relative;\r\n    z-index: 3;\r\n \r\n   \r\n  \r\n   \r\n   \r\n\r\n}\r\n\r\n\r\n.btn{\r\n   \r\n    color: white;\r\n    margin: 1rem 0;\r\n    font-family: fantasy;\r\n    padding: 0.5rem 0;\r\n    text-transform: uppercase;\r\n    outline: none;\r\n    border: none;\r\n    width: 90%;\r\n\r\n}\r\n\r\n\r\n.item-p{\r\n\r\n    width: 100%;\r\n    margin: 1rem 0;\r\n    padding: 0.3rem 0;\r\n}\r\n\r\n\r\n.price{\r\n    background: rgb(171, 206, 206);\r\n    width: 90%;\r\n    margin:  0 auto;\r\n}\r\n\r\n\r\n.preporuka{\r\n\r\n    color: rgb(168, 161, 161);\r\n  margin: 0;\r\n\r\n    font-size: 23px;\r\n    margin-left: 3rem;\r\n    margin-top: 1rem;\r\n\r\n    \r\n\r\n}\r\n\r\n\r\n@media screen and (max-width:750px){\r\n\r\n    .home-items{\r\n        flex-direction: row;\r\n        margin: 0 auto;\r\n        \r\n    \r\n    }\r\n    .items{\r\n        margin: 0 auto;\r\n        width: 40%;\r\n    }\r\n}\r\n\r\n\r\n@media screen and (max-width:490px){\r\n  \r\n\r\n    .home-items{\r\n        flex-direction: column;\r\n    }\r\n\r\n    .items{\r\n        margin: 0 auto;\r\n        width: 90%;\r\n    }\r\n    \r\n    .preporuka{\r\n        margin-left: 1rem;\r\n    }\r\n}\r\n\r\n\r\n.modal{\r\n\tposition: fixed;\r\n\ttext-align: center;\r\n\tpadding: 0.3rem 0;\r\n\tcolor: green;\r\n\twidth: 30%;\r\n\ttop:10px;\r\n\tbackground:wheat;\r\nleft: 35%;\r\nz-index: 9999;\r\n\t\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9sYXB0b3AvbGFwdG9wLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLGdCQUFnQjs7O0FBR2hCO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtHQUN2QixVQUFVO0dBQ1YsY0FBYzs7O0FBR2pCOzs7QUFDQTtJQUNJLGFBQWE7SUFDYixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLDJCQUEyQjtBQUMvQjs7O0FBS0E7O0lBRUksY0FBYztJQUNkLG9DQUFvQzs7SUFFcEMsbUJBQW1COzs7QUFHdkI7OztBQUtBO0lBQ0ksVUFBVTtJQUNWLGtCQUFrQjs7SUFFbEIsb0JBQW9COztJQUVwQixrQkFBa0I7SUFDbEIsVUFBVTs7Ozs7OztBQU9kOzs7QUFFQTs7SUFFSSxZQUFZO0lBQ1osY0FBYztJQUNkLG9CQUFvQjtJQUNwQixpQkFBaUI7SUFDakIseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYixZQUFZO0lBQ1osVUFBVTs7QUFFZDs7O0FBQ0E7O0lBRUksV0FBVztJQUNYLGNBQWM7SUFDZCxpQkFBaUI7QUFDckI7OztBQUdBO0lBQ0ksOEJBQThCO0lBQzlCLFVBQVU7SUFDVixlQUFlO0FBQ25COzs7QUFHQTs7SUFFSSx5QkFBeUI7RUFDM0IsU0FBUzs7SUFFUCxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGdCQUFnQjs7OztBQUlwQjs7O0FBR0E7O0lBRUk7UUFDSSxtQkFBbUI7UUFDbkIsY0FBYzs7O0lBR2xCO0lBQ0E7UUFDSSxjQUFjO1FBQ2QsVUFBVTtJQUNkO0FBQ0o7OztBQUNBOzs7SUFHSTtRQUNJLHNCQUFzQjtJQUMxQjs7SUFFQTtRQUNJLGNBQWM7UUFDZCxVQUFVO0lBQ2Q7O0lBRUE7UUFDSSxpQkFBaUI7SUFDckI7QUFDSjs7O0FBR0E7Q0FDQyxlQUFlO0NBQ2Ysa0JBQWtCO0NBQ2xCLGlCQUFpQjtDQUNqQixZQUFZO0NBQ1osVUFBVTtDQUNWLFFBQVE7Q0FDUixnQkFBZ0I7QUFDakIsU0FBUztBQUNULGFBQWE7O0FBRWIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2xhcHRvcC9sYXB0b3AuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4vKiBpdGVtcyBib3hlcyAqL1xyXG5cclxuXHJcbi5pdGVtLWltZ3tcclxuICAgIGhlaWdodDogMTMwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICB3aWR0aDogNjAlO1xyXG4gICBtYXJnaW46IDAgYXV0bztcclxuXHJcblxyXG59XHJcbi5ob21lLWl0ZW1ze1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgbWFyZ2luOiAycmVtIDNyZW0gMnJlbSAzcmVtO1xyXG59XHJcblxyXG5cclxuXHJcblxyXG4ud3JhcGVye1xyXG4gICBcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgcmdiKDE4NSwgMTk5LCAxOTkpOztcclxuICAgXHJcbiAgICBtYXJnaW46IDAuNXJlbSAwcmVtO1xyXG4gIFxyXG4gICBcclxufVxyXG5cclxuXHJcblxyXG5cclxuLml0ZW1ze1xyXG4gICAgd2lkdGg6IDI1JTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBmb250LWZhbWlseTogZmFudGFzeTtcclxuXHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB6LWluZGV4OiAzO1xyXG4gXHJcbiAgIFxyXG4gIFxyXG4gICBcclxuICAgXHJcblxyXG59XHJcblxyXG4uYnRue1xyXG4gICBcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIG1hcmdpbjogMXJlbSAwO1xyXG4gICAgZm9udC1mYW1pbHk6IGZhbnRhc3k7XHJcbiAgICBwYWRkaW5nOiAwLjVyZW0gMDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuXHJcbn1cclxuLml0ZW0tcHtcclxuXHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbjogMXJlbSAwO1xyXG4gICAgcGFkZGluZzogMC4zcmVtIDA7XHJcbn1cclxuXHJcblxyXG4ucHJpY2V7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMTcxLCAyMDYsIDIwNik7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgbWFyZ2luOiAgMCBhdXRvO1xyXG59XHJcblxyXG5cclxuLnByZXBvcnVrYXtcclxuXHJcbiAgICBjb2xvcjogcmdiKDE2OCwgMTYxLCAxNjEpO1xyXG4gIG1hcmdpbjogMDtcclxuXHJcbiAgICBmb250LXNpemU6IDIzcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogM3JlbTtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcblxyXG4gICAgXHJcblxyXG59XHJcblxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo3NTBweCl7XHJcblxyXG4gICAgLmhvbWUtaXRlbXN7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICBcclxuICAgIFxyXG4gICAgfVxyXG4gICAgLml0ZW1ze1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiA0MCU7XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo0OTBweCl7XHJcbiAgXHJcblxyXG4gICAgLmhvbWUtaXRlbXN7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIH1cclxuXHJcbiAgICAuaXRlbXN7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLnByZXBvcnVrYXtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMXJlbTtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi5tb2RhbHtcclxuXHRwb3NpdGlvbjogZml4ZWQ7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdHBhZGRpbmc6IDAuM3JlbSAwO1xyXG5cdGNvbG9yOiBncmVlbjtcclxuXHR3aWR0aDogMzAlO1xyXG5cdHRvcDoxMHB4O1xyXG5cdGJhY2tncm91bmQ6d2hlYXQ7XHJcbmxlZnQ6IDM1JTtcclxuei1pbmRleDogOTk5OTtcclxuXHRcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/laptop/laptop.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/laptop/laptop.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n <div class=\"loader\" *ngIf=\"loading\">Loading...</div>\n<div class=\"home-items\">\n\n    <div class=\"items\" *ngFor=\"let item of items\">\n     <div class=\"wraper\">\n           \n                       <p class=\"item-p\">{{item.name |uppercase}}</p>\n   \n                       <div [class]=\"item.class\" ><img [src]=\"item.bck\" width=\"100%\" height=\"100%\"></div>\n   \n                       <p class=\"item-p price\">Cena:{{item.price}} RSD</p>\n   \n                       <button class=\"btn detail\"style=\"background:orange\" (click)=\"Details(item)\" [routerLink]=\"['/item', item.rout,item.name,'']\"><span class=\"details-icon\"><i class=\"fas fa-pencil-alt\"></i></span>Detalji</button>\n                     \n                       <button (click)=\"addToCart(item)\" class=\"btn\" [id]=\"item.id\" ><span  class=\"cart-icon\"><i class=\"fas fa-shopping-cart\"></i></span>Dodaj u kantu</button>\n           \n   </div>        \n   </div>\n   </div>\n\n   <div class=\"modal\" *ngIf=\"show\">Dodato u kantu</div>"

/***/ }),

/***/ "./src/app/components/laptop/laptop.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/laptop/laptop.component.ts ***!
  \*******************************************************/
/*! exports provided: LaptopComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LaptopComponent", function() { return LaptopComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_laptop_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/laptop.service */ "./src/app/services/laptop.service.ts");
/* harmony import */ var _services_cart_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/cart.service */ "./src/app/services/cart.service.ts");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);





var LaptopComponent = /** @class */ (function () {
    function LaptopComponent(lapService, cartService) {
        this.lapService = lapService;
        this.cartService = cartService;
        this.loading = true;
    }
    LaptopComponent.prototype.ngOnInit = function () {
        var _this = this;
        axios__WEBPACK_IMPORTED_MODULE_4___default.a.get("https://burger-builder-1232d.firebaseio.com/laptopovi.json")
            .then(function (data) {
            _this.items = data.data;
            _this.loading = false;
            _this.lapService.getLapVal(_this.items);
        });
        // Get value of modal visible(true or  false)
        this.cartService.Modal.subscribe(function (modal) {
            _this.show = modal;
        });
    };
    // Add items to cart..
    LaptopComponent.prototype.addToCart = function (item) {
        this.lapService.addToCart(item);
        this.cartService.add(item);
        // Show modal 
        this.cartService.openModal();
    };
    //  Show details of item
    LaptopComponent.prototype.Details = function (item) {
        this.cartService.Details(item);
        this.lapService.getLap();
        this.lapService.getLapVal(item);
    };
    LaptopComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-laptop',
            template: __webpack_require__(/*! ./laptop.component.html */ "./src/app/components/laptop/laptop.component.html"),
            styles: [__webpack_require__(/*! ./laptop.component.css */ "./src/app/components/laptop/laptop.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_laptop_service__WEBPACK_IMPORTED_MODULE_2__["Laptop"],
            _services_cart_service__WEBPACK_IMPORTED_MODULE_3__["CartService"]])
    ], LaptopComponent);
    return LaptopComponent;
}());



/***/ }),

/***/ "./src/app/components/mobile/mobile.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/mobile/mobile.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n/* items boxes */\r\n\r\n\r\n.item-img{\r\n    height: 130px;\r\n    background-size: cover;\r\n   width: 60%;\r\n   margin: 0 auto;\r\n\r\n\r\n}\r\n\r\n\r\n.home-items{\r\n    display: flex;\r\n    flex-wrap: wrap;\r\n    justify-content: center;\r\n    margin: 2rem 3rem 2rem 3rem;\r\n}\r\n\r\n\r\n.wraper{\r\n   \r\n    margin: 0 auto;\r\n    border: 1px solid rgb(185, 199, 199);;\r\n   \r\n    margin: 0.5rem 0rem;\r\n  \r\n   \r\n}\r\n\r\n\r\n.items{\r\n    width: 25%;\r\n    text-align: center;\r\n\r\n    font-family: fantasy;\r\n\r\n    position: relative;\r\n    z-index: 3;\r\n \r\n   \r\n  \r\n   \r\n   \r\n\r\n}\r\n\r\n\r\n.btn{\r\n\r\n    color: white;\r\n    margin: 1rem 0;\r\n    font-family: fantasy;\r\n    padding: 0.5rem 0;\r\n    text-transform: uppercase;\r\n    outline: none;\r\n    border: none;\r\n    width: 90%;\r\n\r\n}\r\n\r\n\r\n.item-p{\r\n\r\n    width: 100%;\r\n    margin: 1rem 0;\r\n    padding: 0.3rem 0;\r\n}\r\n\r\n\r\n.price{\r\n    background: rgb(171, 206, 206);\r\n    width: 90%;\r\n    margin:  0 auto;\r\n}\r\n\r\n\r\n.preporuka{\r\n\r\n    color: rgb(168, 161, 161);\r\n  margin: 0;\r\n\r\n    font-size: 23px;\r\n    margin-left: 3rem;\r\n    margin-top: 1rem;\r\n\r\n    \r\n\r\n}\r\n\r\n\r\n@media screen and (max-width:750px){\r\n\r\n    .home-items{\r\n        flex-direction: row;\r\n        margin: 0 auto;\r\n        \r\n    \r\n    }\r\n    .items{\r\n        margin: 0 auto;\r\n        width: 40%;\r\n    }\r\n\r\n    .mobile-img{\r\n        width: 35%;\r\n        height: 100px;\r\n    }\r\n}\r\n\r\n\r\n@media screen and (max-width:490px){\r\n  \r\n\r\n    .home-items{\r\n        flex-direction: column;\r\n    }\r\n\r\n    .items{\r\n        margin: 0 auto;\r\n        width: 90%;\r\n    }\r\n    \r\n    .preporuka{\r\n        margin-left: 1rem;\r\n    }\r\n    .mobile-img{\r\n        width: 29%;\r\n        height: 90px;\r\n    }\r\n}\r\n\r\n\r\n.modal{\r\n\tposition: fixed;\r\n\ttext-align: center;\r\n\tpadding: 0.3rem 0;\r\n\tcolor: green;\r\n\twidth: 30%;\r\n\ttop:10px;\r\n\tbackground:wheat;\r\nleft: 35%;\r\nz-index: 9999;\r\n\t\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9tb2JpbGUvbW9iaWxlLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLGdCQUFnQjs7O0FBR2hCO0lBQ0ksYUFBYTtJQUNiLHNCQUFzQjtHQUN2QixVQUFVO0dBQ1YsY0FBYzs7O0FBR2pCOzs7QUFLQTtJQUNJLGFBQWE7SUFDYixlQUFlO0lBQ2YsdUJBQXVCO0lBQ3ZCLDJCQUEyQjtBQUMvQjs7O0FBS0E7O0lBRUksY0FBYztJQUNkLG9DQUFvQzs7SUFFcEMsbUJBQW1COzs7QUFHdkI7OztBQUtBO0lBQ0ksVUFBVTtJQUNWLGtCQUFrQjs7SUFFbEIsb0JBQW9COztJQUVwQixrQkFBa0I7SUFDbEIsVUFBVTs7Ozs7OztBQU9kOzs7QUFFQTs7SUFFSSxZQUFZO0lBQ1osY0FBYztJQUNkLG9CQUFvQjtJQUNwQixpQkFBaUI7SUFDakIseUJBQXlCO0lBQ3pCLGFBQWE7SUFDYixZQUFZO0lBQ1osVUFBVTs7QUFFZDs7O0FBQ0E7O0lBRUksV0FBVztJQUNYLGNBQWM7SUFDZCxpQkFBaUI7QUFDckI7OztBQUdBO0lBQ0ksOEJBQThCO0lBQzlCLFVBQVU7SUFDVixlQUFlO0FBQ25COzs7QUFHQTs7SUFFSSx5QkFBeUI7RUFDM0IsU0FBUzs7SUFFUCxlQUFlO0lBQ2YsaUJBQWlCO0lBQ2pCLGdCQUFnQjs7OztBQUlwQjs7O0FBR0E7O0lBRUk7UUFDSSxtQkFBbUI7UUFDbkIsY0FBYzs7O0lBR2xCO0lBQ0E7UUFDSSxjQUFjO1FBQ2QsVUFBVTtJQUNkOztJQUVBO1FBQ0ksVUFBVTtRQUNWLGFBQWE7SUFDakI7QUFDSjs7O0FBQ0E7OztJQUdJO1FBQ0ksc0JBQXNCO0lBQzFCOztJQUVBO1FBQ0ksY0FBYztRQUNkLFVBQVU7SUFDZDs7SUFFQTtRQUNJLGlCQUFpQjtJQUNyQjtJQUNBO1FBQ0ksVUFBVTtRQUNWLFlBQVk7SUFDaEI7QUFDSjs7O0FBRUE7Q0FDQyxlQUFlO0NBQ2Ysa0JBQWtCO0NBQ2xCLGlCQUFpQjtDQUNqQixZQUFZO0NBQ1osVUFBVTtDQUNWLFFBQVE7Q0FDUixnQkFBZ0I7QUFDakIsU0FBUztBQUNULGFBQWE7O0FBRWIiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL21vYmlsZS9tb2JpbGUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4vKiBpdGVtcyBib3hlcyAqL1xyXG5cclxuXHJcbi5pdGVtLWltZ3tcclxuICAgIGhlaWdodDogMTMwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gICB3aWR0aDogNjAlO1xyXG4gICBtYXJnaW46IDAgYXV0bztcclxuXHJcblxyXG59XHJcblxyXG5cclxuXHJcblxyXG4uaG9tZS1pdGVtc3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIG1hcmdpbjogMnJlbSAzcmVtIDJyZW0gM3JlbTtcclxufVxyXG5cclxuXHJcblxyXG5cclxuLndyYXBlcntcclxuICAgXHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkIHJnYigxODUsIDE5OSwgMTk5KTs7XHJcbiAgIFxyXG4gICAgbWFyZ2luOiAwLjVyZW0gMHJlbTtcclxuICBcclxuICAgXHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5pdGVtc3tcclxuICAgIHdpZHRoOiAyNSU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgZm9udC1mYW1pbHk6IGZhbnRhc3k7XHJcblxyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgei1pbmRleDogMztcclxuIFxyXG4gICBcclxuICBcclxuICAgXHJcbiAgIFxyXG5cclxufVxyXG5cclxuLmJ0bntcclxuXHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbiAgICBtYXJnaW46IDFyZW0gMDtcclxuICAgIGZvbnQtZmFtaWx5OiBmYW50YXN5O1xyXG4gICAgcGFkZGluZzogMC41cmVtIDA7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIHdpZHRoOiA5MCU7XHJcblxyXG59XHJcbi5pdGVtLXB7XHJcblxyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtYXJnaW46IDFyZW0gMDtcclxuICAgIHBhZGRpbmc6IDAuM3JlbSAwO1xyXG59XHJcblxyXG5cclxuLnByaWNle1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDE3MSwgMjA2LCAyMDYpO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuICAgIG1hcmdpbjogIDAgYXV0bztcclxufVxyXG5cclxuXHJcbi5wcmVwb3J1a2F7XHJcblxyXG4gICAgY29sb3I6IHJnYigxNjgsIDE2MSwgMTYxKTtcclxuICBtYXJnaW46IDA7XHJcblxyXG4gICAgZm9udC1zaXplOiAyM3B4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDNyZW07XHJcbiAgICBtYXJnaW4tdG9wOiAxcmVtO1xyXG5cclxuICAgIFxyXG5cclxufVxyXG5cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6NzUwcHgpe1xyXG5cclxuICAgIC5ob21lLWl0ZW1ze1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgXHJcbiAgICBcclxuICAgIH1cclxuICAgIC5pdGVtc3tcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICB3aWR0aDogNDAlO1xyXG4gICAgfVxyXG5cclxuICAgIC5tb2JpbGUtaW1ne1xyXG4gICAgICAgIHdpZHRoOiAzNSU7XHJcbiAgICAgICAgaGVpZ2h0OiAxMDBweDtcclxuICAgIH1cclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjQ5MHB4KXtcclxuICBcclxuXHJcbiAgICAuaG9tZS1pdGVtc3tcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgfVxyXG5cclxuICAgIC5pdGVtc3tcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAucHJlcG9ydWthe1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxcmVtO1xyXG4gICAgfVxyXG4gICAgLm1vYmlsZS1pbWd7XHJcbiAgICAgICAgd2lkdGg6IDI5JTtcclxuICAgICAgICBoZWlnaHQ6IDkwcHg7XHJcbiAgICB9XHJcbn1cclxuXHJcbi5tb2RhbHtcclxuXHRwb3NpdGlvbjogZml4ZWQ7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdHBhZGRpbmc6IDAuM3JlbSAwO1xyXG5cdGNvbG9yOiBncmVlbjtcclxuXHR3aWR0aDogMzAlO1xyXG5cdHRvcDoxMHB4O1xyXG5cdGJhY2tncm91bmQ6d2hlYXQ7XHJcbmxlZnQ6IDM1JTtcclxuei1pbmRleDogOTk5OTtcclxuXHRcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/mobile/mobile.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/mobile/mobile.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"loader\" *ngIf=\"loading\">Loading...</div>\n\n<div class=\"home-items\">\n\n        <div class=\"items\" *ngFor=\"let item of items\">\n         <div class=\"wraper\">\n               \n                           <p class=\"item-p\">{{item.name |uppercase}}</p>\n       \n                           <div [class]=\"item.class\" ><img [src]=\"item.bck\" width=\"100%\" height=\"100%\" ></div>\n       \n                           <p class=\"item-p price\">Cena:{{item.price}} RSD</p>\n       \n                           <button class=\"btn detail\"style=\"background:orange\" (click)=\"Details(item)\" [routerLink]=\"['/item', item.rout,item.name, '']\"><span class=\"details-icon\"><i class=\"fas fa-pencil-alt\"></i></span>Detalji</button>\n                     \n                           <button (click)=\"addToCart(item)\" class=\"btn\" [id]=\"item.id\" ><span  class=\"cart-icon\"><i class=\"fas fa-shopping-cart\"></i></span>Dodaj u korpu</button>\n               \n       </div>        \n       </div>\n       </div>\n       <div class=\"modal\" *ngIf=\"show\">Dodato u kantu</div>"

/***/ }),

/***/ "./src/app/components/mobile/mobile.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/mobile/mobile.component.ts ***!
  \*******************************************************/
/*! exports provided: MobileComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MobileComponent", function() { return MobileComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_mobile_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/mobile.service */ "./src/app/services/mobile.service.ts");
/* harmony import */ var _services_cart_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/cart.service */ "./src/app/services/cart.service.ts");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);





var MobileComponent = /** @class */ (function () {
    function MobileComponent(mobileService, cartService) {
        this.mobileService = mobileService;
        this.cartService = cartService;
        this.loading = true;
    }
    MobileComponent.prototype.ngOnInit = function () {
        // get all items
        var _this = this;
        axios__WEBPACK_IMPORTED_MODULE_4___default.a.get("https://burger-builder-1232d.firebaseio.com/mobilni.json")
            .then(function (data) {
            _this.items = data.data;
            _this.loading = false;
            _this.mobileService.getMobVal(_this.items);
        });
        // Show modal when user add item..
        this.cartService.Modal.subscribe(function (modal) {
            _this.show = modal;
        });
    };
    // Add items to cart..
    MobileComponent.prototype.addToCart = function (item) {
        this.mobileService.addToCart(item);
        this.cartService.add(item);
        // Show modal 
        this.cartService.openModal();
    };
    //  Show details of item
    MobileComponent.prototype.Details = function (item) {
        this.cartService.Details(item);
        this.mobileService.getMob();
        this.mobileService.getMobVal(item);
    };
    MobileComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mobile',
            template: __webpack_require__(/*! ./mobile.component.html */ "./src/app/components/mobile/mobile.component.html"),
            styles: [__webpack_require__(/*! ./mobile.component.css */ "./src/app/components/mobile/mobile.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_mobile_service__WEBPACK_IMPORTED_MODULE_2__["MobileService"],
            _services_cart_service__WEBPACK_IMPORTED_MODULE_3__["CartService"]])
    ], MobileComponent);
    return MobileComponent;
}());



/***/ }),

/***/ "./src/app/components/nav/nav.component.css":
/*!**************************************************!*\
  !*** ./src/app/components/nav/nav.component.css ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "nav{\r\n    height: 60px;\r\n    background: black;\r\n\r\n\r\n}\r\n#responsive-nav{\r\n    position: absolute;\r\n\r\n    left:-60%;\r\n}\r\nul{\r\n    display: flex;\r\n    list-style: none;\r\n}\r\nul li {\r\n\r\nline-height: 60px;\r\ncolor: white;\r\npadding: 0 0.5rem;\r\n\r\n}\r\nul li a{\r\n    color: white;\r\n\r\n    text-transform: uppercase;\r\n    \r\n    text-decoration: none;\r\n}\r\nul li a:hover{\r\n    color: gray;\r\n}\r\n/* cart */\r\n.carts{\r\n\r\n    text-align: center;\r\n  \r\n    font-family: fantasy;\r\n    padding: 0 2rem;\r\n    margin: 1rem auto;\r\n\r\n}\r\n.cart{\r\n    text-align: center;\r\n    width: 300px;\r\n    height: 300px;\r\n\r\n    margin: 0 auto;\r\n    position: absolute;\r\n    right: 0%;\r\n    top:180px;\r\n    z-index: 9999;\r\n    background: gray;\r\n    overflow-y:scroll;\r\n    padding: 1rem;\r\n}\r\n.close{\r\n    position: relative;\r\n    left: 100px;\r\n}\r\nul {\r\n    display: flex;\r\n    list-style:none;\r\n}\r\nul li{\r\n    padding: 0 0.2rem;\r\n}\r\n.cart-img{\r\n    width: 100%;\r\n    height: 100%;\r\n}\r\n.item-name{\r\n    padding: 0.35rem;\r\n    width: 100%;\r\n    text-align: center;\r\n}\r\n.btn{\r\n    padding: 0.3rem;\r\n    outline: none;\r\n    border: none;\r\n    margin: 0.35rem;\r\n}\r\n.price{\r\n    margin: 0.35rem;\r\n}\r\n.burger{\r\n    display: none;\r\n}\r\n.icon-cart{\r\n    padding-right: 5px;\r\n}\r\n@media screen and (max-width:750px){\r\n\r\n    .nav-ul li:not(.burger){\r\n        display: none;\r\n    }\r\n    .cart1{\r\n        display: block;\r\n    }\r\n\r\n.burger{\r\n    display: block;\r\n}\r\n\r\n#responsive-nav{\r\n    background: black;\r\n  \r\n    z-index: 9999;\r\n    width: 60%;\r\n\r\n \r\n\r\n    \r\n \r\n    /* margin: 1rem; */\r\n}\r\n\r\n.animate-nav{\r\n  position: absolute;\r\n  left: -60%;\r\n    -webkit-animation-name: nav;\r\n            animation-name: nav;\r\n    -webkit-animation-duration: 0.5s;\r\n            animation-duration: 0.5s;\r\n    -webkit-animation-fill-mode: forwards;\r\n            animation-fill-mode: forwards;\r\n}\r\n.animate-nav1{\r\n    position: absolute;\r\n    left: 60%;\r\n      -webkit-animation-name: nav1;\r\n              animation-name: nav1;\r\n      -webkit-animation-duration: 0.5s;\r\n              animation-duration: 0.5s;\r\n      -webkit-animation-fill-mode: forwards;\r\n              animation-fill-mode: forwards;\r\n  }\r\n\r\n\r\n  .wid{\r\n      width:0px;\r\n  }\r\n@-webkit-keyframes nav {\r\n\r\nfrom{left: -60%;}\r\nto{left: 0%;}    \r\n}\r\n@keyframes nav {\r\n\r\nfrom{left: -60%;}\r\nto{left: 0%;}    \r\n}\r\n@-webkit-keyframes nav1 {\r\n\r\n    from{left: 0%;}\r\n    to{left: -60%;}    \r\n    }\r\n@keyframes nav1 {\r\n\r\n    from{left: 0%;}\r\n    to{left: -60%;}    \r\n    }\r\n\r\n.responsive-ul{\r\n    flex-direction: column;\r\n    list-style: none;\r\n\r\n}\r\n.responsive-ul li{\r\n    color: white;\r\n}\r\n\r\n\r\n}\r\n@media screen and (max-width:490px){\r\n    .nav-ul li:not(.burger,.cart1){\r\n        display: none;\r\n    }\r\n    .cart1{\r\n \r\n      \r\n        /* position: absolute;\r\n        right: 0%; */\r\n    }\r\n\r\n    .burger{\r\n        display: block;\r\n    }\r\n   \r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9uYXYvbmF2LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxZQUFZO0lBQ1osaUJBQWlCOzs7QUFHckI7QUFDQTtJQUNJLGtCQUFrQjs7SUFFbEIsU0FBUztBQUNiO0FBQ0E7SUFDSSxhQUFhO0lBQ2IsZ0JBQWdCO0FBQ3BCO0FBQ0E7O0FBRUEsaUJBQWlCO0FBQ2pCLFlBQVk7QUFDWixpQkFBaUI7O0FBRWpCO0FBQ0E7SUFDSSxZQUFZOztJQUVaLHlCQUF5Qjs7SUFFekIscUJBQXFCO0FBQ3pCO0FBR0E7SUFDSSxXQUFXO0FBQ2Y7QUFDQSxTQUFTO0FBQ1Q7O0lBRUksa0JBQWtCOztJQUVsQixvQkFBb0I7SUFDcEIsZUFBZTtJQUNmLGlCQUFpQjs7QUFFckI7QUFDQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osYUFBYTs7SUFFYixjQUFjO0lBQ2Qsa0JBQWtCO0lBQ2xCLFNBQVM7SUFDVCxTQUFTO0lBQ1QsYUFBYTtJQUNiLGdCQUFnQjtJQUNoQixpQkFBaUI7SUFDakIsYUFBYTtBQUNqQjtBQUVBO0lBQ0ksa0JBQWtCO0lBQ2xCLFdBQVc7QUFDZjtBQUNBO0lBQ0ksYUFBYTtJQUNiLGVBQWU7QUFDbkI7QUFDQTtJQUNJLGlCQUFpQjtBQUNyQjtBQUNBO0lBQ0ksV0FBVztJQUNYLFlBQVk7QUFDaEI7QUFFQTtJQUNJLGdCQUFnQjtJQUNoQixXQUFXO0lBQ1gsa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSxlQUFlO0lBQ2YsYUFBYTtJQUNiLFlBQVk7SUFDWixlQUFlO0FBQ25CO0FBQ0E7SUFDSSxlQUFlO0FBQ25CO0FBQ0E7SUFDSSxhQUFhO0FBQ2pCO0FBQ0E7SUFDSSxrQkFBa0I7QUFDdEI7QUFFQTs7SUFFSTtRQUNJLGFBQWE7SUFDakI7SUFDQTtRQUNJLGNBQWM7SUFDbEI7O0FBRUo7SUFDSSxjQUFjO0FBQ2xCOztBQUVBO0lBQ0ksaUJBQWlCOztJQUVqQixhQUFhO0lBQ2IsVUFBVTs7Ozs7O0lBTVYsa0JBQWtCO0FBQ3RCOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7SUFDUiwyQkFBbUI7WUFBbkIsbUJBQW1CO0lBQ25CLGdDQUF3QjtZQUF4Qix3QkFBd0I7SUFDeEIscUNBQTZCO1lBQTdCLDZCQUE2QjtBQUNqQztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFNBQVM7TUFDUCw0QkFBb0I7Y0FBcEIsb0JBQW9CO01BQ3BCLGdDQUF3QjtjQUF4Qix3QkFBd0I7TUFDeEIscUNBQTZCO2NBQTdCLDZCQUE2QjtFQUNqQzs7O0VBR0E7TUFDSSxTQUFTO0VBQ2I7QUFDRjs7QUFFQSxLQUFLLFVBQVUsQ0FBQztBQUNoQixHQUFHLFFBQVEsQ0FBQztBQUNaO0FBSkE7O0FBRUEsS0FBSyxVQUFVLENBQUM7QUFDaEIsR0FBRyxRQUFRLENBQUM7QUFDWjtBQUNBOztJQUVJLEtBQUssUUFBUSxDQUFDO0lBQ2QsR0FBRyxVQUFVLENBQUM7SUFDZDtBQUpKOztJQUVJLEtBQUssUUFBUSxDQUFDO0lBQ2QsR0FBRyxVQUFVLENBQUM7SUFDZDs7QUFFSjtJQUNJLHNCQUFzQjtJQUN0QixnQkFBZ0I7O0FBRXBCO0FBQ0E7SUFDSSxZQUFZO0FBQ2hCOzs7QUFHQTtBQUNBO0lBQ0k7UUFDSSxhQUFhO0lBQ2pCO0lBQ0E7OztRQUdJO29CQUNZO0lBQ2hCOztJQUVBO1FBQ0ksY0FBYztJQUNsQjs7QUFFSiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbmF2L25hdi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsibmF2e1xyXG4gICAgaGVpZ2h0OiA2MHB4O1xyXG4gICAgYmFja2dyb3VuZDogYmxhY2s7XHJcblxyXG5cclxufVxyXG4jcmVzcG9uc2l2ZS1uYXZ7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcblxyXG4gICAgbGVmdDotNjAlO1xyXG59XHJcbnVse1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbn1cclxudWwgbGkge1xyXG5cclxubGluZS1oZWlnaHQ6IDYwcHg7XHJcbmNvbG9yOiB3aGl0ZTtcclxucGFkZGluZzogMCAwLjVyZW07XHJcblxyXG59XHJcbnVsIGxpIGF7XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcblxyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIFxyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG59XHJcblxyXG5cclxudWwgbGkgYTpob3ZlcntcclxuICAgIGNvbG9yOiBncmF5O1xyXG59XHJcbi8qIGNhcnQgKi9cclxuLmNhcnRze1xyXG5cclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBcclxuICAgIGZvbnQtZmFtaWx5OiBmYW50YXN5O1xyXG4gICAgcGFkZGluZzogMCAycmVtO1xyXG4gICAgbWFyZ2luOiAxcmVtIGF1dG87XHJcblxyXG59XHJcbi5jYXJ0e1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgaGVpZ2h0OiAzMDBweDtcclxuXHJcbiAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHJpZ2h0OiAwJTtcclxuICAgIHRvcDoxODBweDtcclxuICAgIHotaW5kZXg6IDk5OTk7XHJcbiAgICBiYWNrZ3JvdW5kOiBncmF5O1xyXG4gICAgb3ZlcmZsb3cteTpzY3JvbGw7XHJcbiAgICBwYWRkaW5nOiAxcmVtO1xyXG59XHJcblxyXG4uY2xvc2V7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBsZWZ0OiAxMDBweDtcclxufVxyXG51bCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgbGlzdC1zdHlsZTpub25lO1xyXG59XHJcbnVsIGxpe1xyXG4gICAgcGFkZGluZzogMCAwLjJyZW07XHJcbn1cclxuLmNhcnQtaW1ne1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDEwMCU7XHJcbn1cclxuXHJcbi5pdGVtLW5hbWV7XHJcbiAgICBwYWRkaW5nOiAwLjM1cmVtO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLmJ0bntcclxuICAgIHBhZGRpbmc6IDAuM3JlbTtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBib3JkZXI6IG5vbmU7XHJcbiAgICBtYXJnaW46IDAuMzVyZW07XHJcbn1cclxuLnByaWNle1xyXG4gICAgbWFyZ2luOiAwLjM1cmVtO1xyXG59XHJcbi5idXJnZXJ7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcbi5pY29uLWNhcnR7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbn1cclxuXHJcbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6NzUwcHgpe1xyXG5cclxuICAgIC5uYXYtdWwgbGk6bm90KC5idXJnZXIpe1xyXG4gICAgICAgIGRpc3BsYXk6IG5vbmU7XHJcbiAgICB9XHJcbiAgICAuY2FydDF7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB9XHJcblxyXG4uYnVyZ2Vye1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbiNyZXNwb25zaXZlLW5hdntcclxuICAgIGJhY2tncm91bmQ6IGJsYWNrO1xyXG4gIFxyXG4gICAgei1pbmRleDogOTk5OTtcclxuICAgIHdpZHRoOiA2MCU7XHJcblxyXG4gXHJcblxyXG4gICAgXHJcbiBcclxuICAgIC8qIG1hcmdpbjogMXJlbTsgKi9cclxufVxyXG5cclxuLmFuaW1hdGUtbmF2e1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBsZWZ0OiAtNjAlO1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IG5hdjtcclxuICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogMC41cztcclxuICAgIGFuaW1hdGlvbi1maWxsLW1vZGU6IGZvcndhcmRzO1xyXG59XHJcbi5hbmltYXRlLW5hdjF7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBsZWZ0OiA2MCU7XHJcbiAgICAgIGFuaW1hdGlvbi1uYW1lOiBuYXYxO1xyXG4gICAgICBhbmltYXRpb24tZHVyYXRpb246IDAuNXM7XHJcbiAgICAgIGFuaW1hdGlvbi1maWxsLW1vZGU6IGZvcndhcmRzO1xyXG4gIH1cclxuXHJcblxyXG4gIC53aWR7XHJcbiAgICAgIHdpZHRoOjBweDtcclxuICB9XHJcbkBrZXlmcmFtZXMgbmF2IHtcclxuXHJcbmZyb217bGVmdDogLTYwJTt9XHJcbnRve2xlZnQ6IDAlO30gICAgXHJcbn1cclxuQGtleWZyYW1lcyBuYXYxIHtcclxuXHJcbiAgICBmcm9te2xlZnQ6IDAlO31cclxuICAgIHRve2xlZnQ6IC02MCU7fSAgICBcclxuICAgIH1cclxuXHJcbi5yZXNwb25zaXZlLXVse1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcblxyXG59XHJcbi5yZXNwb25zaXZlLXVsIGxpe1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG5cclxufVxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjQ5MHB4KXtcclxuICAgIC5uYXYtdWwgbGk6bm90KC5idXJnZXIsLmNhcnQxKXtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG4gICAgLmNhcnQxe1xyXG4gXHJcbiAgICAgIFxyXG4gICAgICAgIC8qIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICByaWdodDogMCU7ICovXHJcbiAgICB9XHJcblxyXG4gICAgLmJ1cmdlcntcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIH1cclxuICAgXHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/nav/nav.component.html":
/*!***************************************************!*\
  !*** ./src/app/components/nav/nav.component.html ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav (dragover)=\"drag()\">\n\n  <ul class=\"nav-ul\">\n        <li (click)=\"hideCart()\" ><a href=\"#\" routerLink=\"/\" *ngIf=\"router.url !== '/'\"><i class=\"fas fa-home\" style=\"padding:0 0.3rem\" ></i> Pocetna strana</a></li>\n      <li  (click)=\"hide()\"><a href=\"#\" routerLink=\"/televizori\"><i class=\"fas fa-tv\" style=\"padding:0 0.3rem\"  ></i> Televizori</a></li>\n      <li  (click)=\"hide()\"><a href=\"#\" routerLink=\"/mobilni\"><i class=\"fas fa-mobile-alt\" style=\"padding:0 0.3rem\"  ></i>Mobilni </a></li>\n      <li  (click)=\"hide()\" ><a href=\"#\" routerLink=\"/laptopovi\"><i class=\"fas fa-laptop\" style=\"padding:0 0.3rem\"  ></i>Laptopovi </a></li>\n     \n      <li><span><i class=\"fa fa-shopping-cart icon-cart\"   (mouseenter)=\"show()\" routerLink=\"/cart\" (click)=\"hide()\" ></i></span>Total {{toTal }} RSD ({{(number)}})</li>\n      <li (click)=\"toogleMenu()\" class=\"burger\"><span><i class=\"fas fa-bars\"></i> </span></li>\n      \n\n  </ul>\n\n</nav>\n<div  id=\"responsive-nav\"> \n<ul class=\"responsive-ul\" >\n<li (click)=\"hideCart()\" ><a href=\"#\" routerLink=\"/\" *ngIf=\"router.url !== '/'\"><i class=\"fas fa-home\" style=\"padding:0 0.3rem\"></i> Pocetna strana</a></li>\n<li  (click)=\"hideCart()\"><a href=\"#\" routerLink=\"/televizori\"><i class=\"fas fa-tv\" style=\"padding:0 0.3rem\"></i> Televizori</a></li>\n<li  (click)=\"hideCart()\"><a href=\"#\" routerLink=\"/mobilni\"><i class=\"fas fa-mobile-alt\" style=\"padding:0 0.3rem\"></i>Mobilni </a></li>\n<li  (click)=\"hideCart()\"><a href=\"#\" routerLink=\"/laptopovi\"><i class=\"fas fa-laptop\" style=\"padding:0 0.3rem\"></i>Laptopovi </a></li>\n<li><span><i class=\"fa fa-shopping-cart\"   (click)=\"show()\" (mouseleave)=\"hide()\" (click)=\"hideCart()\"routerLink=\"/cart\" (click)=\"hide()\"></i>Total{{toTal}}RSD({{(number)}})</span></li>\n</ul>\n</div>\n\n\n<div class=\"cart\" [hidden]=\"toogle\">\n  <span class=\"close\" (click)=\"hide()\" >X</span>\n      <p  style=\"text-align: center;\" *ngIf=\"cartitem.length!==0\">  Total {{toTal}} RSD ({{number}})</p>\n\n    \n\n      \n      <div class=\"carts zero\" *ngFor=\"let cartite of cartitem\">\n          \n        <div *ngIf=\"cartitem.length>0\">\n \n\n             <p class=\"item-name\">Name: {{cartite.name |uppercase}}</p>\n\n            <div class=\"color\" ><img [src]=\"cartite.bck\" class=\"cart-img\"></div>\n            <p class=\"price\">Price: {{cartite.price | currency:USD}}</p> \n            \n             <button (click)=\"deleteFromCart(cartite)\" class=\"btn\" style=\"background:red\">Izbaci iz kante</button>\n        </div>\n\n      \n   \n</div>\n<p *ngIf=\"cartitem.length===0\">Emty Cart</p>\n</div>\n\n"

/***/ }),

/***/ "./src/app/components/nav/nav.component.ts":
/*!*************************************************!*\
  !*** ./src/app/components/nav/nav.component.ts ***!
  \*************************************************/
/*! exports provided: NavComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavComponent", function() { return NavComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/cart.service */ "./src/app/services/cart.service.ts");
/* harmony import */ var _services_tv_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/tv.service */ "./src/app/services/tv.service.ts");
/* harmony import */ var _services_laptop_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/laptop.service */ "./src/app/services/laptop.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_mobile_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/mobile.service */ "./src/app/services/mobile.service.ts");







var NavComponent = /** @class */ (function () {
    function NavComponent(cartService, tvService, lapService, mobileService, router) {
        this.cartService = cartService;
        this.tvService = tvService;
        this.lapService = lapService;
        this.mobileService = mobileService;
        this.router = router;
        this.toogle = true;
        this.tooglemenu = true;
        this.hideHome = true;
        this.toTal = 0;
        this.cartitem = [];
        this.number = 0;
    }
    NavComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.cartitem = this.cartService.cart;
        this.cartService.totalCart.subscribe(function (data) {
            _this.toTal = data;
            _this.number = _this.cartitem.length;
        });
    };
    NavComponent.prototype.deleteFromCart = function (item) {
        this.cartService.del(item);
    };
    NavComponent.prototype.hideCart = function () {
        var ul = document.getElementById("responsive-nav");
        ul.className = "wid";
        this.tooglemenu = true;
    };
    NavComponent.prototype.toogleMenu = function () {
        this.tooglemenu = !this.tooglemenu;
        var ul = document.getElementById("responsive-nav");
        if (!this.tooglemenu) {
            ul.className = "animate-nav";
        }
        else {
            ul.className = "animate-nav1";
        }
    };
    NavComponent.prototype.show = function () {
        this.toogle = false;
    };
    NavComponent.prototype.hide = function () {
        this.toogle = true;
    };
    NavComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-nav',
            template: __webpack_require__(/*! ./nav.component.html */ "./src/app/components/nav/nav.component.html"),
            styles: [__webpack_require__(/*! ./nav.component.css */ "./src/app/components/nav/nav.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"],
            _services_tv_service__WEBPACK_IMPORTED_MODULE_3__["TvService"],
            _services_laptop_service__WEBPACK_IMPORTED_MODULE_4__["Laptop"],
            _services_mobile_service__WEBPACK_IMPORTED_MODULE_6__["MobileService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], NavComponent);
    return NavComponent;
}());



/***/ }),

/***/ "./src/app/components/slider/slider.component.css":
/*!********************************************************!*\
  !*** ./src/app/components/slider/slider.component.css ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n\r\n.slider{\r\n    height: 280px;\r\n  \r\n    position: relative;\r\n    width: 100%;\r\n   \r\n}\r\n\r\n\r\n\r\n\r\n.slides{\r\nposition: relative;\r\nleft: 0;\r\ntop: 0;\r\nheight: 100%;\r\nwidth: 100%;\r\n\r\n\r\n\r\n\r\n}\r\n\r\n\r\n\r\n\r\n.animate{\r\n    \r\n\r\n\r\n    -webkit-animation-name: slider;\r\n    \r\n\r\n\r\n            animation-name: slider;\r\n    -webkit-animation-duration: 0.7s;\r\n            animation-duration: 0.7s;\r\n    opacity: 1;\r\n    \r\n\r\n}\r\n\r\n\r\n\r\n\r\n@-webkit-keyframes slider{\r\n    from { opacity: 0}\r\n    to {\r\n        opacity: 1;\r\n    }\r\n}\r\n\r\n\r\n\r\n\r\n@keyframes slider{\r\n    from { opacity: 0}\r\n    to {\r\n        opacity: 1;\r\n    }\r\n}\r\n\r\n\r\n\r\n\r\n#btn{\r\n    position: absolute;\r\n    z-index: 9000;\r\n    right: 5%;\r\n    top: 50%;\r\n  \r\n}\r\n\r\n\r\n\r\n\r\n#btn1{\r\n    position: absolute;\r\n    z-index: 999;\r\n    left: 5%;\r\n    top:50%;\r\n\r\n}\r\n\r\n\r\n\r\n\r\nbutton i{\r\n    font-size: 30px;\r\n}\r\n\r\n\r\n\r\n\r\nbutton{\r\n    -webkit-transform: translate(-50%,-50%);\r\n            transform: translate(-50%,-50%);\r\n    background: transparent;\r\n    border: none;\r\n    outline: none;\r\n}\r\n\r\n\r\n\r\n\r\n.vise{\r\n    position: absolute;\r\n    bottom: 20px;\r\n    left: 7rem;\r\n    background: yellow;\r\n    height:30px;\r\n    line-height: 30px;\r\n    border-radius: 7px;\r\n    z-index: 3;\r\n\r\n}\r\n\r\n\r\n\r\n\r\n.random-img{\r\n    position: absolute;\r\n    top: 50%;\r\n    left: 50%;\r\n    -webkit-transform: translate(-50%,-50%);\r\n            transform: translate(-50%,-50%);\r\n    height: 150px;\r\n    width: 250px;\r\n}\r\n\r\n\r\n\r\n\r\n.random-img img{\r\n    height: 100%;\r\n    width: 100%;\r\n}\r\n\r\n\r\n\r\n\r\n.name{\r\n    position: absolute;\r\n    text-align: center;\r\n    width: 100%;\r\n    left: 0;\r\n    top: 0.3rem;\r\n\r\n    z-index: 3;\r\n\r\n\r\n\r\n}\r\n\r\n\r\n\r\n\r\n@media screen and (max-width:790px){\r\n    .slider{\r\n        height: 300px;\r\n    }\r\n\r\n}\r\n\r\n\r\n\r\n\r\n@media screen and (max-width:490px){\r\n    .slider{\r\n        height: 200px;\r\n    }\r\n    button i{\r\n        font-size: 20px;\r\n    }\r\n    .vise{\r\n        left: 0.3rem;\r\n        bottom: 0.5rem;\r\n    }\r\n .random-img{\r\n     top: 60%;\r\n     height: 130px;\r\n     width: 150px;\r\n }\r\n    .random-img img{\r\n       \r\n        height: 80%;\r\n        width: 100%\r\n        \r\n    }\r\n    .name{\r\n        font-size: 25px;\r\n    }\r\n    \r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9zbGlkZXIvc2xpZGVyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFFQTtJQUNJLGFBQWE7O0lBRWIsa0JBQWtCO0lBQ2xCLFdBQVc7O0FBRWY7Ozs7O0FBS0E7QUFDQSxrQkFBa0I7QUFDbEIsT0FBTztBQUNQLE1BQU07QUFDTixZQUFZO0FBQ1osV0FBVzs7Ozs7QUFLWDs7Ozs7QUFJQTs7OztJQUlJLDhCQUFzQjs7OztZQUF0QixzQkFBc0I7SUFDdEIsZ0NBQXdCO1lBQXhCLHdCQUF3QjtJQUN4QixVQUFVOzs7QUFHZDs7Ozs7QUFFQTtJQUNJLE9BQU8sVUFBVTtJQUNqQjtRQUNJLFVBQVU7SUFDZDtBQUNKOzs7OztBQUxBO0lBQ0ksT0FBTyxVQUFVO0lBQ2pCO1FBQ0ksVUFBVTtJQUNkO0FBQ0o7Ozs7O0FBTUE7SUFDSSxrQkFBa0I7SUFDbEIsYUFBYTtJQUNiLFNBQVM7SUFDVCxRQUFROztBQUVaOzs7OztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFlBQVk7SUFDWixRQUFRO0lBQ1IsT0FBTzs7QUFFWDs7Ozs7QUFDQTtJQUNJLGVBQWU7QUFDbkI7Ozs7O0FBSUE7SUFDSSx1Q0FBK0I7WUFBL0IsK0JBQStCO0lBQy9CLHVCQUF1QjtJQUN2QixZQUFZO0lBQ1osYUFBYTtBQUNqQjs7Ozs7QUFFQTtJQUNJLGtCQUFrQjtJQUNsQixZQUFZO0lBQ1osVUFBVTtJQUNWLGtCQUFrQjtJQUNsQixXQUFXO0lBQ1gsaUJBQWlCO0lBQ2pCLGtCQUFrQjtJQUNsQixVQUFVOztBQUVkOzs7OztBQUNBO0lBQ0ksa0JBQWtCO0lBQ2xCLFFBQVE7SUFDUixTQUFTO0lBQ1QsdUNBQStCO1lBQS9CLCtCQUErQjtJQUMvQixhQUFhO0lBQ2IsWUFBWTtBQUNoQjs7Ozs7QUFFQTtJQUNJLFlBQVk7SUFDWixXQUFXO0FBQ2Y7Ozs7O0FBRUE7SUFDSSxrQkFBa0I7SUFDbEIsa0JBQWtCO0lBQ2xCLFdBQVc7SUFDWCxPQUFPO0lBQ1AsV0FBVzs7SUFFWCxVQUFVOzs7O0FBSWQ7Ozs7O0FBRUE7SUFDSTtRQUNJLGFBQWE7SUFDakI7O0FBRUo7Ozs7O0FBRUE7SUFDSTtRQUNJLGFBQWE7SUFDakI7SUFDQTtRQUNJLGVBQWU7SUFDbkI7SUFDQTtRQUNJLFlBQVk7UUFDWixjQUFjO0lBQ2xCO0NBQ0g7S0FDSSxRQUFRO0tBQ1IsYUFBYTtLQUNiLFlBQVk7Q0FDaEI7SUFDRzs7UUFFSSxXQUFXO1FBQ1g7O0lBRUo7SUFDQTtRQUNJLGVBQWU7SUFDbkI7O0FBRUoiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL3NsaWRlci9zbGlkZXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG5cclxuLnNsaWRlcntcclxuICAgIGhlaWdodDogMjgwcHg7XHJcbiAgXHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgXHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi5zbGlkZXN7XHJcbnBvc2l0aW9uOiByZWxhdGl2ZTtcclxubGVmdDogMDtcclxudG9wOiAwO1xyXG5oZWlnaHQ6IDEwMCU7XHJcbndpZHRoOiAxMDAlO1xyXG5cclxuXHJcblxyXG5cclxufVxyXG5cclxuXHJcblxyXG4uYW5pbWF0ZXtcclxuICAgIFxyXG5cclxuXHJcbiAgICBhbmltYXRpb24tbmFtZTogc2xpZGVyO1xyXG4gICAgYW5pbWF0aW9uLWR1cmF0aW9uOiAwLjdzO1xyXG4gICAgb3BhY2l0eTogMTtcclxuICAgIFxyXG5cclxufVxyXG5cclxuQGtleWZyYW1lcyBzbGlkZXJ7XHJcbiAgICBmcm9tIHsgb3BhY2l0eTogMH1cclxuICAgIHRvIHtcclxuICAgICAgICBvcGFjaXR5OiAxO1xyXG4gICAgfVxyXG59XHJcblxyXG5cclxuXHJcblxyXG5cclxuI2J0bntcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHotaW5kZXg6IDkwMDA7XHJcbiAgICByaWdodDogNSU7XHJcbiAgICB0b3A6IDUwJTtcclxuICBcclxufVxyXG4jYnRuMXtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIHotaW5kZXg6IDk5OTtcclxuICAgIGxlZnQ6IDUlO1xyXG4gICAgdG9wOjUwJTtcclxuXHJcbn1cclxuYnV0dG9uIGl7XHJcbiAgICBmb250LXNpemU6IDMwcHg7XHJcbn1cclxuXHJcblxyXG5cclxuYnV0dG9ue1xyXG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwtNTAlKTtcclxuICAgIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgb3V0bGluZTogbm9uZTtcclxufVxyXG5cclxuLnZpc2V7XHJcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICBib3R0b206IDIwcHg7XHJcbiAgICBsZWZ0OiA3cmVtO1xyXG4gICAgYmFja2dyb3VuZDogeWVsbG93O1xyXG4gICAgaGVpZ2h0OjMwcHg7XHJcbiAgICBsaW5lLWhlaWdodDogMzBweDtcclxuICAgIGJvcmRlci1yYWRpdXM6IDdweDtcclxuICAgIHotaW5kZXg6IDM7XHJcblxyXG59XHJcbi5yYW5kb20taW1ne1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiA1MCU7XHJcbiAgICBsZWZ0OiA1MCU7XHJcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLC01MCUpO1xyXG4gICAgaGVpZ2h0OiAxNTBweDtcclxuICAgIHdpZHRoOiAyNTBweDtcclxufVxyXG5cclxuLnJhbmRvbS1pbWcgaW1ne1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5uYW1le1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgdG9wOiAwLjNyZW07XHJcblxyXG4gICAgei1pbmRleDogMztcclxuXHJcblxyXG5cclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo3OTBweCl7XHJcbiAgICAuc2xpZGVye1xyXG4gICAgICAgIGhlaWdodDogMzAwcHg7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOjQ5MHB4KXtcclxuICAgIC5zbGlkZXJ7XHJcbiAgICAgICAgaGVpZ2h0OiAyMDBweDtcclxuICAgIH1cclxuICAgIGJ1dHRvbiBpe1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgIH1cclxuICAgIC52aXNle1xyXG4gICAgICAgIGxlZnQ6IDAuM3JlbTtcclxuICAgICAgICBib3R0b206IDAuNXJlbTtcclxuICAgIH1cclxuIC5yYW5kb20taW1ne1xyXG4gICAgIHRvcDogNjAlO1xyXG4gICAgIGhlaWdodDogMTMwcHg7XHJcbiAgICAgd2lkdGg6IDE1MHB4O1xyXG4gfVxyXG4gICAgLnJhbmRvbS1pbWcgaW1ne1xyXG4gICAgICAgXHJcbiAgICAgICAgaGVpZ2h0OiA4MCU7XHJcbiAgICAgICAgd2lkdGg6IDEwMCVcclxuICAgICAgICBcclxuICAgIH1cclxuICAgIC5uYW1le1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcclxuICAgIH1cclxuICAgIFxyXG59Il19 */"

/***/ }),

/***/ "./src/app/components/slider/slider.component.html":
/*!*********************************************************!*\
  !*** ./src/app/components/slider/slider.component.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n\n<div class=\"slider\" >\n      <h1 class=\"name\">{{name}}</h1>\n      <button (click)=\"previus()\" id=\"btn1\"><i class=\"fas fa-arrow-left\"></i></button>\n      <button (click)=\"next()\" id=\"btn\"><i class=\"fas fa-arrow-right\"></i></button>\n      <div class=\"slides\" [ngStyle]=\"setMyStyles()\" id=\"slider\" *ngIf=\"router.url==='/'\"></div>\n                                             \n<div class=\"vise\">{{vise}}</div>\n<div class=\"random-img\"><img [src]=\"img\"></div>\n</div>\n"

/***/ }),

/***/ "./src/app/components/slider/slider.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/slider/slider.component.ts ***!
  \*******************************************************/
/*! exports provided: SliderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SliderComponent", function() { return SliderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_slider_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/slider.service */ "./src/app/services/slider.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var SliderComponent = /** @class */ (function () {
    function SliderComponent(sliderService, router) {
        this.sliderService = sliderService;
        this.router = router;
        this.iter = 0;
        this.img = [];
        this.name = [];
    }
    SliderComponent.prototype.ngOnInit = function () {
        var slides = this.sliderService.getSlides();
        this.slides = slides;
        this.first = slides["bck"][0];
        this.vise = slides["vise"];
        this.img = slides["item"][0];
        this.name = slides["name"][0];
        console.log(this.name);
        this.animate();
    };
    SliderComponent.prototype.next = function () {
        var _this = this;
        this.iter++;
        if (this.iter > this.slides["bck"].length - 1) {
            this.iter = 0;
        }
        this.first = this.slides["bck"][this.iter];
        this.img = this.slides["item"][this.iter];
        this.name = this.slides["name"][this.iter];
        var slider = document.getElementById("slider");
        slider.classList.add("animate");
        setTimeout(function () {
            slider.classList.remove("animate");
        }, 700);
        clearInterval(this.loadingTimer);
        this.loadingTimer = setInterval(function () { _this.sliderChange(); }, 4000);
    };
    SliderComponent.prototype.previus = function () {
        var _this = this;
        this.iter--;
        if (this.iter < 0) {
            this.iter = this.slides["bck"].length - 1;
        }
        this.first = this.slides["bck"][this.iter];
        this.img = this.slides["item"][this.iter];
        this.name = this.slides["name"][this.iter];
        var slider = document.getElementById("slider");
        slider.classList.add("animate");
        setTimeout(function () {
            slider.classList.remove("animate");
        }, 1100);
        clearInterval(this.loadingTimer);
        this.loadingTimer = setInterval(function () { _this.sliderChange(); }, 4000);
    };
    SliderComponent.prototype.animate = function () {
        var _this = this;
        this.loadingTimer = setInterval(function () { _this.sliderChange(); }, 4000);
    };
    SliderComponent.prototype.sliderChange = function () {
        if (this.router.url === "/") {
            this.iter++;
            if (this.iter > this.slides["bck"].length - 1) {
                this.iter = 0;
            }
            this.first = this.slides["bck"][this.iter];
            this.img = this.slides["item"][this.iter];
            this.name = this.slides["name"][this.iter];
            var slider = document.getElementById("slider");
            slider.classList.add("animate");
            slider.classList.remove("animate");
        }
    };
    SliderComponent.prototype.setMyStyles = function () {
        var styles = {
            'background': this.first,
        };
        return styles;
    };
    SliderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-slider',
            template: __webpack_require__(/*! ./slider.component.html */ "./src/app/components/slider/slider.component.html"),
            styles: [__webpack_require__(/*! ./slider.component.css */ "./src/app/components/slider/slider.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_slider_service__WEBPACK_IMPORTED_MODULE_2__["SliderService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], SliderComponent);
    return SliderComponent;
}());



/***/ }),

/***/ "./src/app/components/tv/tv.component.css":
/*!************************************************!*\
  !*** ./src/app/components/tv/tv.component.css ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n/* items boxes */\r\n\r\n\r\n.item-img{\r\n    height: 130px;\r\n    background-size: cover;\r\n   width: 60%;\r\n   margin: 0 auto;\r\n\r\n\r\n}\r\n\r\n\r\n.home-items{\r\n    display: flex;\r\n    flex-wrap: wrap;\r\n    justify-content: center;\r\n    margin: 2rem 3rem 2rem 3rem;\r\n}\r\n\r\n\r\n.wraper{\r\n   \r\n    margin: 0 auto;\r\n    border: 1px solid rgb(185, 199, 199);;\r\n   \r\n    margin: 0.5rem 0rem;\r\n  \r\n   \r\n}\r\n\r\n\r\n.items{\r\n    width: 25%;\r\n    text-align: center;\r\n\r\n    font-family: fantasy;\r\n\r\n    position: relative;\r\n    z-index: 3;\r\n \r\n   \r\n  \r\n   \r\n   \r\n\r\n}\r\n\r\n\r\n.btn{\r\n    \r\n    color: white;\r\n    margin: 1rem 0;\r\n    font-family: fantasy;\r\n    padding: 0.5rem 0;\r\n    text-transform: uppercase;\r\n    outline: none;\r\n    border: none;\r\n    width: 90%;\r\n\r\n}\r\n\r\n\r\n.item-p{\r\n\r\n    width: 100%;\r\n    margin: 1rem 0;\r\n    padding: 0.3rem 0;\r\n}\r\n\r\n\r\n.price{\r\n    background: rgb(171, 206, 206);\r\n    width: 90%;\r\n    margin:  0 auto;\r\n}\r\n\r\n\r\n.preporuka{\r\n\r\n    color: rgb(168, 161, 161);\r\n  margin: 0;\r\n\r\n    font-size: 23px;\r\n    margin-left: 3rem;\r\n    margin-top: 1rem;\r\n\r\n    \r\n\r\n}\r\n\r\n\r\n@media screen and (max-width:750px){\r\n\r\n    .home-items{\r\n        flex-direction: row;\r\n        margin: 0 auto;\r\n        \r\n    \r\n    }\r\n    .items{\r\n        margin: 0 auto;\r\n        width: 40%;\r\n    }\r\n}\r\n\r\n\r\n@media screen and (max-width:490px){\r\n  \r\n\r\n    .home-items{\r\n        flex-direction: column;\r\n    }\r\n\r\n    .items{\r\n        margin: 0 auto;\r\n        width: 90%;\r\n    }\r\n    \r\n    .preporuka{\r\n        margin-left: 1rem;\r\n    }\r\n}\r\n\r\n\r\n.modal{\r\n\tposition: fixed;\r\n\ttext-align: center;\r\n\tpadding: 0.3rem 0;\r\n\tcolor: green;\r\n\twidth: 30%;\r\n\ttop:10px;\r\n\tbackground:wheat;\r\nleft: 35%;\r\nz-index: 9999;\r\n\t\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy90di90di5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSxnQkFBZ0I7OztBQUdoQjtJQUNJLGFBQWE7SUFDYixzQkFBc0I7R0FDdkIsVUFBVTtHQUNWLGNBQWM7OztBQUdqQjs7O0FBQ0E7SUFDSSxhQUFhO0lBQ2IsZUFBZTtJQUNmLHVCQUF1QjtJQUN2QiwyQkFBMkI7QUFDL0I7OztBQUtBOztJQUVJLGNBQWM7SUFDZCxvQ0FBb0M7O0lBRXBDLG1CQUFtQjs7O0FBR3ZCOzs7QUFLQTtJQUNJLFVBQVU7SUFDVixrQkFBa0I7O0lBRWxCLG9CQUFvQjs7SUFFcEIsa0JBQWtCO0lBQ2xCLFVBQVU7Ozs7Ozs7QUFPZDs7O0FBRUE7O0lBRUksWUFBWTtJQUNaLGNBQWM7SUFDZCxvQkFBb0I7SUFDcEIsaUJBQWlCO0lBQ2pCLHlCQUF5QjtJQUN6QixhQUFhO0lBQ2IsWUFBWTtJQUNaLFVBQVU7O0FBRWQ7OztBQUNBOztJQUVJLFdBQVc7SUFDWCxjQUFjO0lBQ2QsaUJBQWlCO0FBQ3JCOzs7QUFHQTtJQUNJLDhCQUE4QjtJQUM5QixVQUFVO0lBQ1YsZUFBZTtBQUNuQjs7O0FBR0E7O0lBRUkseUJBQXlCO0VBQzNCLFNBQVM7O0lBRVAsZUFBZTtJQUNmLGlCQUFpQjtJQUNqQixnQkFBZ0I7Ozs7QUFJcEI7OztBQUdBOztJQUVJO1FBQ0ksbUJBQW1CO1FBQ25CLGNBQWM7OztJQUdsQjtJQUNBO1FBQ0ksY0FBYztRQUNkLFVBQVU7SUFDZDtBQUNKOzs7QUFDQTs7O0lBR0k7UUFDSSxzQkFBc0I7SUFDMUI7O0lBRUE7UUFDSSxjQUFjO1FBQ2QsVUFBVTtJQUNkOztJQUVBO1FBQ0ksaUJBQWlCO0lBQ3JCO0FBQ0o7OztBQUdBO0NBQ0MsZUFBZTtDQUNmLGtCQUFrQjtDQUNsQixpQkFBaUI7Q0FDakIsWUFBWTtDQUNaLFVBQVU7Q0FDVixRQUFRO0NBQ1IsZ0JBQWdCO0FBQ2pCLFNBQVM7QUFDVCxhQUFhOztBQUViIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy90di90di5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi8qIGl0ZW1zIGJveGVzICovXHJcblxyXG5cclxuLml0ZW0taW1ne1xyXG4gICAgaGVpZ2h0OiAxMzBweDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgIHdpZHRoOiA2MCU7XHJcbiAgIG1hcmdpbjogMCBhdXRvO1xyXG5cclxuXHJcbn1cclxuLmhvbWUtaXRlbXN7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC13cmFwOiB3cmFwO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBtYXJnaW46IDJyZW0gM3JlbSAycmVtIDNyZW07XHJcbn1cclxuXHJcblxyXG5cclxuXHJcbi53cmFwZXJ7XHJcbiAgIFxyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMTg1LCAxOTksIDE5OSk7O1xyXG4gICBcclxuICAgIG1hcmdpbjogMC41cmVtIDByZW07XHJcbiAgXHJcbiAgIFxyXG59XHJcblxyXG5cclxuXHJcblxyXG4uaXRlbXN7XHJcbiAgICB3aWR0aDogMjUlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIGZvbnQtZmFtaWx5OiBmYW50YXN5O1xyXG5cclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgIHotaW5kZXg6IDM7XHJcbiBcclxuICAgXHJcbiAgXHJcbiAgIFxyXG4gICBcclxuXHJcbn1cclxuXHJcbi5idG57XHJcbiAgICBcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIG1hcmdpbjogMXJlbSAwO1xyXG4gICAgZm9udC1mYW1pbHk6IGZhbnRhc3k7XHJcbiAgICBwYWRkaW5nOiAwLjVyZW0gMDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG4gICAgd2lkdGg6IDkwJTtcclxuXHJcbn1cclxuLml0ZW0tcHtcclxuXHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIG1hcmdpbjogMXJlbSAwO1xyXG4gICAgcGFkZGluZzogMC4zcmVtIDA7XHJcbn1cclxuXHJcblxyXG4ucHJpY2V7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMTcxLCAyMDYsIDIwNik7XHJcbiAgICB3aWR0aDogOTAlO1xyXG4gICAgbWFyZ2luOiAgMCBhdXRvO1xyXG59XHJcblxyXG5cclxuLnByZXBvcnVrYXtcclxuXHJcbiAgICBjb2xvcjogcmdiKDE2OCwgMTYxLCAxNjEpO1xyXG4gIG1hcmdpbjogMDtcclxuXHJcbiAgICBmb250LXNpemU6IDIzcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogM3JlbTtcclxuICAgIG1hcmdpbi10b3A6IDFyZW07XHJcblxyXG4gICAgXHJcblxyXG59XHJcblxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo3NTBweCl7XHJcblxyXG4gICAgLmhvbWUtaXRlbXN7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICBcclxuICAgIFxyXG4gICAgfVxyXG4gICAgLml0ZW1ze1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiA0MCU7XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDo0OTBweCl7XHJcbiAgXHJcblxyXG4gICAgLmhvbWUtaXRlbXN7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIH1cclxuXHJcbiAgICAuaXRlbXN7XHJcbiAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLnByZXBvcnVrYXtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMXJlbTtcclxuICAgIH1cclxufVxyXG5cclxuXHJcbi5tb2RhbHtcclxuXHRwb3NpdGlvbjogZml4ZWQ7XHJcblx0dGV4dC1hbGlnbjogY2VudGVyO1xyXG5cdHBhZGRpbmc6IDAuM3JlbSAwO1xyXG5cdGNvbG9yOiBncmVlbjtcclxuXHR3aWR0aDogMzAlO1xyXG5cdHRvcDoxMHB4O1xyXG5cdGJhY2tncm91bmQ6d2hlYXQ7XHJcbmxlZnQ6IDM1JTtcclxuei1pbmRleDogOTk5OTtcclxuXHRcclxufSJdfQ== */"

/***/ }),

/***/ "./src/app/components/tv/tv.component.html":
/*!*************************************************!*\
  !*** ./src/app/components/tv/tv.component.html ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"loader\" *ngIf=\"loading\">Loading...</div>\n\n   <div class=\"modal\" *ngIf=\"show\">Dodato u kantu</div>\n\n<div class=\"home-items\">\n\n        <div class=\"items\" *ngFor=\"let item of items\">\n         <div class=\"wraper\">\n               \n                           <p class=\"item-p\">{{item.name |uppercase}}</p>\n       \n                           <div [class]=\"item.class\" ><img [src]=\"item.bck\" width=\"100%\" height=\"100%\"></div>\n       \n                           <p class=\"item-p price\">Cena:{{item.price}} RSD</p>\n       \n            \n                         \n                    <button class=\"btn detail\"style=\"background:orange\" (click)=\"Details(item)\" [routerLink]=\"['/item', item.rout,item.name,'']\"><span class=\"details-icon\"><i class=\"fas fa-pencil-alt\"></i></span>Detalji</button>\n                     \n                    <button (click)=\"addToCart(item)\" class=\"btn\" [id]=\"item.id\" ><span  class=\"cart-icon\"><i class=\"fas fa-shopping-cart\"></i></span>Dodaj u kantu</button>\n               \n       </div>        \n       </div>\n       </div>\n       \n"

/***/ }),

/***/ "./src/app/components/tv/tv.component.ts":
/*!***********************************************!*\
  !*** ./src/app/components/tv/tv.component.ts ***!
  \***********************************************/
/*! exports provided: TvComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TvComponent", function() { return TvComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_cart_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/cart.service */ "./src/app/services/cart.service.ts");
/* harmony import */ var _services_tv_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/tv.service */ "./src/app/services/tv.service.ts");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_4__);





var TvComponent = /** @class */ (function () {
    function TvComponent(tvService, cartService) {
        this.tvService = tvService;
        this.cartService = cartService;
        this.items = [];
        this.spectv = [];
        this.loading = true;
    }
    TvComponent.prototype.ngOnInit = function () {
        var _this = this;
        axios__WEBPACK_IMPORTED_MODULE_4___default.a.get("https://burger-builder-1232d.firebaseio.com/tv.json")
            .then(function (data) {
            _this.items = data.data;
            _this.loading = false;
            _this.tvService.getTvVal(_this.items);
        });
        // Show modal when user add item..
        this.cartService.Modal.subscribe(function (modal) {
            _this.show = modal;
        });
    };
    // Add items to cart..
    TvComponent.prototype.addToCart = function (item) {
        this.tvService.addToCart(item);
        this.cartService.add(item);
        // Show modal 
        this.cartService.openModal();
    };
    //  Show details of item
    TvComponent.prototype.Details = function (item) {
        this.cartService.Details(item);
        this.tvService.getTvVal(item);
        this.tvService.getTV();
    };
    TvComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-tv',
            template: __webpack_require__(/*! ./tv.component.html */ "./src/app/components/tv/tv.component.html"),
            styles: [__webpack_require__(/*! ./tv.component.css */ "./src/app/components/tv/tv.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_tv_service__WEBPACK_IMPORTED_MODULE_3__["TvService"],
            _services_cart_service__WEBPACK_IMPORTED_MODULE_2__["CartService"]])
    ], TvComponent);
    return TvComponent;
}());



/***/ }),

/***/ "./src/app/services/cart.service.ts":
/*!******************************************!*\
  !*** ./src/app/services/cart.service.ts ***!
  \******************************************/
/*! exports provided: CartService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CartService", function() { return CartService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/Subject */ "./node_modules/rxjs-compat/_esm5/Subject.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");




var CartService = /** @class */ (function () {
    function CartService() {
        this.modal = true;
        this.cart = [];
        this.totalInCart = 0;
        // Get single item(Details)
        this.singleItemSubj = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](this.item);
        this.singleItem = this.singleItemSubj.asObservable();
        // Get home items value
        this.homeItems = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](this.items);
        this.homes = this.homeItems.asObservable();
        // Modal subject
        this.Modal = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // Get total amount  from cart
        this.totalCart = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        // Get a number of items in cart
        this.lenC = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.data = new rxjs_Subject__WEBPACK_IMPORTED_MODULE_2__["Subject"]();
        this.Modal.next(this.modal);
    }
    //  Opem modal for a custom messages...
    CartService.prototype.openModal = function () {
        var _this = this;
        this.modal = true;
        this.Modal.next(this.modal);
        setTimeout(function () {
            _this.modal = false;
            _this.Modal.next(_this.modal);
        }, 3000);
    };
    // Reset cart to 0 items
    CartService.prototype.resetCart = function () {
        this.cart.splice(0, this.cart.length);
        var num = 0;
        this.cart.forEach(function (total) {
            num += total.price;
        });
        this.totalInCart = num;
        this.lenCart = this.cart.length;
        this.totalCart.next(this.totalInCart);
        this.lenC.next(this.lenCart);
    };
    //Get Items
    //Details (lead to specific item)
    CartService.prototype.Details = function (item) {
        this.singleItemSubj.next(item);
    };
    // Get home items vrednosti for a single 
    CartService.prototype.getHomeVal = function (item) {
        this.homeItems.next(item.vrednosti);
    };
    //Add item
    CartService.prototype.add = function (item) {
        this.cart.push(item);
        var num = 0;
        this.cart.forEach(function (total) {
            num += total.price;
        });
        this.totalInCart = num;
        this.totalCart.next(this.totalInCart);
    };
    //Delete item 
    CartService.prototype.del = function (item) {
        this.cart.splice(this.cart.indexOf(item), 1);
        var num = 0;
        this.cart.forEach(function (total) {
            num += total.price;
        });
        this.totalInCart = num;
        this.lenCart = this.cart.length;
        this.totalCart.next(this.totalInCart);
        this.lenC.next(this.lenCart);
    };
    CartService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], CartService);
    return CartService;
}());



/***/ }),

/***/ "./src/app/services/header.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/header.service.ts ***!
  \********************************************/
/*! exports provided: HeaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderService", function() { return HeaderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HeaderService = /** @class */ (function () {
    function HeaderService() {
        this.colors = ["#330000", "#800000", "#ff0000", "#5900b3", "#2e2eb8", "#7070db", "#0000ff", "#33ccff", "#009900", "#ffff00"];
    }
    HeaderService.prototype.getColors = function () {
        return this.colors;
    };
    HeaderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HeaderService);
    return HeaderService;
}());



/***/ }),

/***/ "./src/app/services/laptop.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/laptop.service.ts ***!
  \********************************************/
/*! exports provided: Laptop */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Laptop", function() { return Laptop; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var Laptop = /** @class */ (function () {
    function Laptop() {
        this.x = [];
        this.speclap = ["Ekran", "Procesor", "Graficka Karta", "HDD", "USB 3.1", "Boja", "Bluetooth"];
        this.lapItem = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.speclap);
        this.lap = this.lapItem.asObservable();
        this.lapItems = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.items);
        this.laps = this.lapItems.asObservable();
    }
    Laptop.prototype.addToCart = function (item) {
        this.x.push(item);
    };
    Laptop.prototype.getLap = function () {
        this.lapItem.next(this.speclap);
    };
    Laptop.prototype.getLapVal = function (vrednosti) {
        this.lapItems.next(vrednosti);
    };
    Laptop.prototype.getRout = function () {
        for (var i = 0; i < this.items.length; i++) {
            return this.items[i].rout;
        }
    };
    Laptop = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], Laptop);
    return Laptop;
}());



/***/ }),

/***/ "./src/app/services/mobile.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/mobile.service.ts ***!
  \********************************************/
/*! exports provided: MobileService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MobileService", function() { return MobileService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var MobileService = /** @class */ (function () {
    function MobileService() {
        this.specmob = ["Dizajn", "Tip ekrana", "Ekran osetljiv na dodir", "Dijagonala ekrana", "Rezolucija", "Procesor", "Sistem"];
        this.x = [];
        this.mobItem = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.specmob);
        this.mob = this.mobItem.asObservable();
        this.mobItems = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.items);
        this.mobs = this.mobItems.asObservable();
    }
    MobileService.prototype.addToCart = function (item) {
        this.x.push(item);
    };
    MobileService.prototype.getMob = function () {
        this.mobItem.next(this.specmob);
    };
    MobileService.prototype.getMobVal = function (item) {
        this.mobItems.next(item);
    };
    MobileService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], MobileService);
    return MobileService;
}());



/***/ }),

/***/ "./src/app/services/slider.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/slider.service.ts ***!
  \********************************************/
/*! exports provided: SliderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SliderService", function() { return SliderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var SliderService = /** @class */ (function () {
    function SliderService() {
        this.slides = {
            bck: ["white", "white", "white"],
            vise: "saznaj vise...",
            item: ["../../../assets/lap3.jpg", "../../../assets/mobile3.jpg", "../../../assets/tv3.jpg"],
            name: ["SAMSUNG", "SAMSUNG S10", "VOX"]
        };
    }
    SliderService.prototype.getSlides = function () {
        return this.slides;
    };
    SliderService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], SliderService);
    return SliderService;
}());



/***/ }),

/***/ "./src/app/services/tv.service.ts":
/*!****************************************!*\
  !*** ./src/app/services/tv.service.ts ***!
  \****************************************/
/*! exports provided: TvService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TvService", function() { return TvService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");



var TvService = /** @class */ (function () {
    function TvService() {
        this.spectv = ["Ekran", "Dijagonala", "Rezolucija", "Android", "Smart Tv", "Boja", "Godina Proizvodnje"];
        this.x = [];
        this.tvItem = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.spectv);
        this.tv = this.tvItem.asObservable();
        this.tvItems = new rxjs__WEBPACK_IMPORTED_MODULE_2__["BehaviorSubject"](this.items);
        this.tvs = this.tvItems.asObservable();
    }
    TvService.prototype.addToCart = function (item) {
        this.x.push(item);
    };
    TvService.prototype.getTV = function () {
        this.tvItem.next(this.spectv);
    };
    TvService.prototype.getTvVal = function (vrednosti) {
        this.tvItems.next(vrednosti);
    };
    TvService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TvService);
    return TvService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Korisnik\Desktop\diplomski\addToCart\addToCart\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map